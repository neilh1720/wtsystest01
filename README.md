Production Release historically  in dev\weathertrak\released\MFG\EOL_wtSysTest    
Fut: For production release move to Bitbucket when controller moves 

**Release 3.1.05 - Board Test for manufacturing functional testing with PIC32 rel7.10**
  Board Test by subcontractors for PIC32 Control/7000L01 Capacitive Touch/7000L03 LC18 Term Board/706LC10
  .cfg expanded to include 3 functional board tests
  finer granular testing options for different functional board tests
  generate serialNumber.txt typicall in shortcuts start folder ~ C:\Testfiles\EOL_test
  improved error response for USB plugin
  added easy rel_XXX to support easy release of package
  added folder .\windows_shortcuts and updated spec on how to invoke them from 
  C:\Testfiles\EOL_test\dist\wtSysTest.exe -d PRO3_H2O
  ...
  C:\Testfiles\EOL_test\dist\wtSysTest.exe -d 7000L01_TEST_BOARD
  C:\Testfiles\EOL_test\dist\wtSysTest.exe -d 7000L03_TEST_BOARD
  C:\Testfiles\EOL_test\dist\wtSysTest.exe -d 706LC10_TEST_BOARD
  
**Release 3.0.02 - PRO3-2W direct for PIC32 rel7.10**
  Gave it a new release to show incompatible with ealier rel7.8
  Made it c
  fixed an LC18 decision current reading issue
  fixed an LC18/LC36 flow flowConst multiplier
  updated screen formats to fit on smaller screen sizes in manufacturing
  internally refactored to have HwModel2W2G to cover all versions of _H2O (2W2G)
  added a 1sec userability pause when Valve test sucessfully completes to see screen
  Updated Hydropoint Manufacturing Support wtSysTest.docx: Building a release 
  
**Release 2.1.02 - PRO3-2W direct**
For [PRO3_H2O] change internal processing from PRO_2W2G
Add support .ini header
[OPTIXR_H2O]

Add .ini line "version_rel" and on power up check for version - eg 7.10

**Release 2.1.02 - PRO3-2W direct**
     Supports PRO3-H20 (or baseline 2WDirect   
  -  PRO3-H20 mode supports 4 flow requests   
      PRO3-H20 testing, H20 specific station screen & flow   
	       Activatte sequentially 4MV valves and apply range mask to  individual current, using station simulator loading   
	       Activate sequentially 1PS valve and  and apply range mask to  individual current, using station simulator loading   
			Activate 1 station valve over 2W and apply range mask to reported current, using station R on PCB.   
			
  Status:   
     done: pic32 LED test extend for H20 in PIC32, One  LED D42 not used/tested. Future firmware will have LEDs test cmd     
	  -done: 2W-direct D9- valve/ps shift register bit, &  2W D42 "2Wire Status" turn 2W path power on/off to test   
	  - 2W D43 ("2W Err") not testable   
	  - 2W D14 D15 D18 D21 FlowX (not sw testable) tested with Test Head Flow Emulation.   

     Valves/Stations updated for 2W2G case only.   
      done: H20 MV1-4 set and read current intergration with controller PIC32 working.   
	  done: H20 PS set and read current working - requires MSP430 v111 or later   
	  done: intergrate LC36 opts   
	  done: LC36 tested 36stations+MV   
	  Station Valve - sets.    
	  done: py & pic32, need new H20 current read.   
	  done: py, change screens for station test to MV1-4, PS   
	   done: H20 read 1 flow. Verified that flow 1-4 on PIC32 works.   
	  done:py, extend py for H20 flow1-4, need to check against real values   
	  done:py. RAINSWITCH - check two states OFF & ON   
	  done:py check how SLOTs is passed/failed   

Testing    
   - PRO3_2WG2 - stnTst:inProg , leds:yes   
     LC36  - stnTst:yes   
	 LC18  - stnTst:yes   
	  
**Proto Release 2.1.x -Barcode scanner**   
Support the "Read Board Serial Numbers" button. One serial number can be input from barcode scanner or manually.   
Future: Determine how many boards to track. Need to design dialog to track them    

Release 2.0.5 LC18   
Detect release of PIC32, and cope with no release supported   
For Triton release 7.9, speed up LC18 station test by implementing a current avg on PIC32. Support wtSysTest current avg for LC18.   

Release 2.0.4 LC18   
Make the beep length shorter.   

Release 2.0.3 LC18   
Modifications based on manucfacturing release feedback   

Release 2.0.2 LC18   
Release for manucfacturing EOL LC18 test   
Added KeyboardTestBeepOn - beeps per key tested.   

Release 2.0.1 LC18   
Enlarge Buttons type   
LC18 minor error handling fixes   
Added LC36 support  testStation - added Ignore for testStation. Requires 7.9, and just released 7.8   

Release 2.0.0 LC18     

https://bitbucket.org/neilh1720/wtsystest01/src/master/   hydropoint login in via atlassian.com   
superseded Perforce dev\weathertrak\wip\nh\code\wtSysTest   

---
## Comments that 
**Bold**
*Italic*

[Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), 
 [add, commit,](https://confluence.atlassian.com/x/8QhODQ) 
  [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).