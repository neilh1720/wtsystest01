#----------------------------------------------------------------------
# A Hydropoint Manufacturing Test Program 
#----------------------------------------------------------------------
import sys
import os
import os.path
#import glob
import wx
from gui import MyFrameSysTest as MyFrame
#from gui import MyPanelKeyboard as MyPanelKey
import logging
import serial           #as mySerial
import serial.threaded  #as myThreaded
import threading
import time
import datetime
from hy_protocol import HYEC01
#import hy_actions
#import Queue
import queue
import struct
import binascii
import argparse
#from optparse import OptionParser #depreciated 3.2
from  configparser import ConfigParser

from inq_enum import *
from serial.tools.list_ports_windows import NULL
import csv
import json
#from __builtin__ import True
#from wx.lib.agw.cubecolourdialog import dlg
#from types import NoneType
#import types

__version__='3.1.05'
# globals  - could go in config.py
# Decouple the window events to a seperate thread   

######
# hwModel supported
hwModelEqu = 0
HwModelPRO3 = 0
HwModelLC18 = 1
HwModelLC36 = 2
#HSame as _H2O
HwModel2W2G = 3
#HwModelOPTIXR_H2O =4
HwModelTxt = ['PRO3','LC18','LC36','H20']
######
# testModel supported
TestModelEqu = 0
TestModel_EOL =0
TestModelLC18_BRD =1
TestModelCapactive_BRD =2
TestModelController_BRD =3

TestModelNoCpuTest = [TestModelCapactive_BRD,TestModelController_BRD]

gSerialPortCom='' #leave empty for finding first port available
gCfgFileNameDefault='hydropoint-manufacturing.cfg'
gHydrLogFileNam="HydropointLog"  #edefaults to .txt 
gHydrLog=None
gHydr_csv_file=None
#Core Display Device Config
gCustPcCode1=2
gCustPcCode2=0
gCustCompanyName='Hydropoint Data Systems'
#The following may need condensing to a number string no hyprnd
gCust800Num='1-800-362-8774'
#Core Display Data
gCustSn='000'
gModemSimSn='000'
#VerDutStr=''
gVerDutList={0}
gVerDutMjInt=99
gVerDutMnInt=99
gVerTgtRel ={0}  # a list with two parts of rel eg 7,10
gCustFactorConfig={1,2,3}
#
gAsk_snA = '000'

gFrame=None
gSerHandle=None
gKeyPressed_Count=0
gKeyReportComplete=0

#######
# Standard options - can be changed by .cfg
gControlBoardTest=True
gSerialNumberWr=True #write card serial number to serialnumber.txt 
gModemSimTest=False  #attempt a read of ModemSimTest
gStationKeysTest=True
gBeepTest=True
gLedsTest=True
gRainswitchTest=True
gKeyboardRead=24   #0 for none, 1 for any 1, 24 for std keyboard size.
gKeyboardTestBeepOn=True #False
gStationTestAuto=True #if false notifys user to do manual station test
gValveTest=True
gFlowTest=True
gConfigureSystem=True
gReadBoardSerialNum=True
#can be changed at [common] and each board
gStationtestMax_ma=800
gStationtestMin_ma=100
gMvtestMax_ma=500
gMvtestMin_ma=50

#######
#List of UI tabs
uiTabSystemTests_Num=0
uiTabConfigure_Num =1
uiTabKeyReport_Num=2
uiTabStationReport_Num=3
#list of Console Status indications
UiConsoleStatus_DEFAULT=0  
UiConsoleStatus_FAIL=1
UiConsoleStatus_PASS=2
HSUCCESS =0
HFAIL = 1
dut_specification ='Default'

        
class Mywin(MyFrame):
    """
    Process events from gui
    All gui is encapsulated
    """
    buttonAction =0
    gLine=0
    
    def __init__(self, parent):
        #super(MyFrame, self).__init__(parent) #this doesn't let .app exit
        MyFrame.__init__(self,parent)
        #self.Bind(wx.EVT_CLOSE, self.closeWindow)  #Bind the EVT_CLOSE event to closeWindow()

    #def closeWindow(self, event):
    #    self.Destroy() #Should close the app .

    #def __del__( self ):
    #    pass
    def ShowMessageUser(self,msgStr):
        #dlg = wx.ScrolledMessageDialog(parent, message, caption)
        wx.MessageBox(str(msgStr), 'Review', 
            wx.OK | wx.ICON_INFORMATION)      

    def UserDialogAction(self,msgStr):
        #dlg = wx.ScrolledMessageDialog(parent, message, caption)
        wx.MessageBox(str(msgStr), 'Action', 
            wx.OK | wx.ICON_INFORMATION)     
        
    def UserDialogOK(parent, message, caption = 'Dialog Press OK'):
        dlg = wx.MessageDialog(parent, message, caption,  wx.OK | wx.ICON_INFORMATION)
        dlg.ShowModal()
        dlg.Destroy()
    
    def UserDialogYesNo(parent, message, caption = 'Dialog Yes/No'):
        dlg = wx.MessageDialog(parent, message, caption,  wx.YES_NO | wx.ICON_QUESTION)
        questionRsp_bool =dlg.ShowModal() == wx.ID_YES
        dlg.Destroy()
        return questionRsp_bool    
    
    def UserTextEntryDialog(parent=None, message='askMsg', caption = 'When complete, Press OK or <enter>'):
        dlg2 = wx.TextEntryDialog(parent,message,caption, 'Enter Serial Number')
        #dlg = wx.MessageDialog(parent, message, caption,  wx.OK | wx.ICON_INFORMATION)
        ui_text = "NoSerialNumberRecorded"
        ui_status=dlg2.ShowModal()
        if ui_status == wx.ID_OK:
            ui_text = dlg2.GetValue()

        dlg2.Destroy()
        return ui_text
    
    #experimental -doesn't work
    #def UserDialogKeyTest(parent, message="UserDialogKeyTest KeyboardTest"):
        #ut_dlg = DialogKeyboard(parent).ShowModal()
        #print(ut_dlg)
    
    def uiConsole(self,infoStr,status=0) :
        try:
            self.gLine = self.gLine+1
            out_info = str(self.gLine)+":"+str(infoStr)
            logging.info("uiConsole("+str(status)+"):"+out_info)

            # Set textAttr then Append Text
            if UiConsoleStatus_FAIL== status :
                self.m_uiConsole1.SetDefaultStyle(wx.TextAttr(wx.RED))
                #self.debug_exception
            elif UiConsoleStatus_PASS==status :
                self.m_uiConsole1.SetDefaultStyle(wx.TextAttr(wx.BLACK))
                #self.m_uiConsole1.SetForegroundColour(wx.GREEN)
            elif UiConsoleStatus_DEFAULT==status :
                self.m_uiConsole1.SetDefaultStyle(wx.TextAttr(wx.BLACK))
            self.m_uiConsole1.AppendText('\n'+out_info)
            
        except AttributeError:
            logging.info("uiConsole attributeError:"+ str(infoStr)) #as method does not exist send stdout
        todaysDate =datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d')
        sttimeSec = datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S')
        gHydroLog.writerow([todaysDate,sttimeSec,gCustSn,infoStr])
        gHydr_csv_file.flush()
    
    def OnConfigureReadBtn(self,event):
        inUi_queue.put_nowait(INQ_GET_READ_CONTEXT_CMD)
        pass
    
    def OnConfigureWriteBtn(self,event):
        #self.uiConsole('OnConfigureWriteBtn')
        inUi_queue.put_nowait(INQ_SET_WRITE_CONTEXT_CMD) 
        pass
    
    def OnConfigureSystemDetailsBtn(self, event):
        #self.uiConsole('OnConfigureSystemDetailBtn')
        inUi_queue.put_nowait(INQ_GET_CONFIGURE_SYSTEM_DETAILS_CMD) 
        #inUi_queue.put_nowait(INQ_GET_READ_CONTEXT_CMD)
        pass
    
    def OnDefaultEepromBtn(self, event):
        #self.uiConsole('OnConfigureSystemBtn')
        inUi_queue.put_nowait(INQ_SET_DEFAULT_EEPROM_CMD) 
        #inUi_queue.put_nowait(INQ_GET_READ_CONTEXT_CMD)
        pass
    
    def OnTimeToClose(self, evt):
        """Event handler for the button click."""
        #print "See ya later!"
        self.Close()

        
    #the following event handlers don't exist at this level
    def OnSerialNumButton(self, evt):
        """Event handler for the button click."""
        inUi_queue.put_nowait(INQ_GET_SERIAL_NUMBER_CMD)

    #def OnModifyContextButton(self, evt):
    #    """Event handler for the button click."""
    #    inUi_queue.put_nowait(INQ_GET_MODIFY_CONTEXT_CMD)

    def OnRainSwitchTestBtn(self, evt):
        """Event handler for RainSwitchTest vesion click."""
        inUi_queue.put_nowait(INQ_GET_RAIN_SWITCH_CMD)
        
    def OnGetModemBtn(self, evt):
        """Event handler for GetModem vesion click."""
        inUi_queue.put_nowait(INQ_GET_SIM_CARD_NUMBER_CMD)

    def OnDoLedsTestBtn(self, evt):
        """Event handler for DoLeds click."""
        inUi_queue.put_nowait(INQ_TURN_ON_LEDS_CMD)

    #def OnBeepButton(self, evt):
    def OnDoBeepTestBtn(self, evt):
        """Event handler for the beep click."""
        inUi_queue.put_nowait(INQ_SET_BEEPER_CMD)
        
    def OnDoKeysTestBtn(self, evt):
        """Event handler for DoKeys click."""
        inUi_queue.put_nowait(INQ_DO_CAPTOUCH_TEST_CMD)

    def OnDoStationTestBtn(self, evt):
        """Event handler for DoStation Test click."""
        inUi_queue.put_nowait(INQ_DO_STATION_TEST_CMD)


    def OnGetFlowBtn(self, evt):
        """Event handler for TestAll click."""
        inUi_queue.put_nowait(INQ_GET_FLOW_CMD)

    def OnSysTestConfigureSystemBtn(self, evt):
        """Event handler for Configure System click."""
        inUi_queue.put_nowait(INQ_CONFIGURE_SYSTEM_CMD)
        
    def OnTestAllBtn(self, evt):
        """Event handler for TestAll click."""
        inUi_queue.put_nowait(INQ_DO_ALL_SYSTESTS_CMD)

    def OnReadBoardSerialNumbers(self, event):
        #MyFrame.OnReadBoardSerialNumbers(self, event)
        """Event handler for READ_SERIALNUMBERS click."""
        #Need to collect user input while in windows main thread.
        #Need custom window with mutipile fields mapping to cards
        inUi_queue.put_nowait(INQ_READ_SERIALNUMBERS_CMD)

    def OnKeyReportChoice(self, evt):
        global gKeyReportComplete
        keyChoice = self.m_choiceKeyReport1.GetSelection()
        gKeyReportComplete=10+keyChoice
        #self.UserDialogOK( "KeyReport["+str(keyChoice)+"]", caption = 'KeyReport Test Choice')
        logging.debug('User KeyReportChoice: '+str(gKeyReportComplete)+',')

    #def OnSoundKeyCheckBoxEvt(self, evt):
    #    gUserSoundKeyFailEvt = True
    #    logging.debug('User OnSoundKeyCheckBoxEvt. '+str(gUserSoundKeyFailEvt))

        
    def OnNotebook1PageChanging(self, evt): 
        # Page Changing - read from page 
        global gKeyReportComplete
        Notebook1CurrentPg = gFrame.m_notebook1.GetSelection()
        if uiTabKeyReport_Num ==Notebook1CurrentPg:
            gKeyReportComplete = 1
        logging.debug('User Notebook1PageChanging: '+str(Notebook1CurrentPg)+', KeyReport '+str(gKeyReportComplete)+',')
        
    def OnNotebook1PageChanged(self, evt): 
        # Assymed only generated if user caused event
        Notebook1NewPg = gFrame.m_notebook1.GetSelection()
        if uiTabKeyReport_Num ==Notebook1NewPg:
            gFrame.m_choiceKeyReport1.Show(False)
        logging.debug('User Notebook1PageChanged: '+str(Notebook1NewPg)+',')

        
def timeoutCheck(resp1,infoStr):
    if [] != resp1 :
        #print("timeoutCheck1 "+str(resp1))
        #    gFrame.ShowMessageUser(infoStr+' TypeNone')
        #    os._exit(1)
        if 'timeout' == resp1:
            gFrame.ShowMessageUser(infoStr)
            logging.debug("timeoutCheck exit")
            os._exit(1)
    else:
        logging.debug("timeoutCheck2 "+str(type(resp1))+" resp1:"+ ' '.join(resp1))

def cardSnCheck():
    global gCustSn
    respAttn=hym.hyAttn()
    CustSn = hym.getSn()
    if CustSn!=gCustSn:
        oldCustSn = gCustSn
        gCustSn=CustSn
        gFrame.uiConsole('New card "'+gCustSn+'" Was "'+oldCustSn+'")')
     
def getModemInfo():
    #cardSnCheck()  
    #time.sleep(2)  
    #Get ID could be Modem ImeiSn  "AT+CGSN" ImeiSn AT#CGSN BoardSn AT+GSN 
    modemSN=hym.getModemSn('AT+CGSN')
    time.sleep(1)
    modemSN=hym.getModemSn('AT+CGSN')          
    gFrame.uiConsole('modemIMEI='+modemSN+';') 
    #
    # Fails with AT+GMM - ModelID
    # CGMR - Software revision
    # Fails with AT+CCID  - SID based on
    #modemCCID=hym.getModemSn('AT+CCID')
    #gFrame.uiConsole('modemSim='+modemCCID+';') 
    #modemGMM=hym.getModemATresp()
    #gFrame.uiConsole('modemModelId='+modemGMM+';')     
    #respSimCardNumber="Not Detected"
    #respSimCardNumber=hym.getSimCardNumber() # - not responding
    #gFrame.uiConsole("ModemSim: "+str(respSimCardNumber),2)
    #
def stationValveReportInit():
    if HwModel2W2G== hwModelEqu :
        stationValveReportTxt_2W2G()
    elif HwModelLC18 == hwModelEqu :
        stationValveReportTxtLC18()
    else :
    #if HwModelLC18 == hwModelEqu :
    #elif HwModelLC36 == hwModelEqu :
        stationValveReportTxtLC36()


def stationValveReportTxt_2W2G():
    fontPointSz=16
    gFrame.m_textSrMV1.SetLabel(' ')
    gFrame.m_textSrMV2.SetLabel(' ')
    gFrame.m_textSrMV3.SetLabel(' ')
    gFrame.m_textSrMV4.SetLabel(' ')
    
    #gFrame.m_textSrPS1.SetLabel('PS1')
    gFrame.m_textSr01.SetLabel('Stn1')
    gFrame.m_textSr02.SetLabel(' ')
    gFrame.m_textSr03.SetLabel(' ')
    gFrame.m_textSr04.SetLabel(' ')
    gFrame.m_textSr05.SetLabel(' ')    
    gFrame.m_textSr06.SetLabel(' ')
    gFrame.m_textSr07.SetLabel(' ')
    gFrame.m_textSr08.SetLabel(' ')
    gFrame.m_textSr09.SetLabel(' ')
    gFrame.m_textSr10.SetLabel('MV1')
    gFrame.m_textSr11.SetLabel('MV2')
    gFrame.m_textSr12.SetLabel('MV3')
    gFrame.m_textSr13.SetLabel('MV4')
    gFrame.m_textSr14.SetLabel(' ')
    gFrame.m_textSr15.SetLabel(' ')
    gFrame.m_textSr16.SetLabel(' ')
    gFrame.m_textSr17.SetLabel(' ')
    gFrame.m_textSr18.SetLabel(' ')
    gFrame.m_textSr19.SetLabel('PS1')
    gFrame.m_textSr20.SetLabel(' ')
    gFrame.m_textSr21.SetLabel(' ')
    gFrame.m_textSr22.SetLabel(' ')
    gFrame.m_textSr23.SetLabel(' ')
    gFrame.m_textSr24.SetLabel(' ')
    gFrame.m_textSr25.SetLabel(' ')
    gFrame.m_textSr26.SetLabel(' ')
    gFrame.m_textSr27.SetLabel(' ')
    gFrame.m_textSr28.SetLabel(' ')
    gFrame.m_textSr29.SetLabel(' ')
    gFrame.m_textSr30.SetLabel(' ')
    gFrame.m_textSr31.SetLabel(' ')
    gFrame.m_textSr32.SetLabel(' ')
    gFrame.m_textSr33.SetLabel(' ')
    gFrame.m_textSr34.SetLabel(' ')
    gFrame.m_textSr35.SetLabel(' ')
    gFrame.m_textSr36.SetLabel(' ')  

def stationValveReportTxtLC18():
    fontPointSz=16

    gFrame.m_textSrMV1.SetLabel('MV1')
    gFrame.m_textSrMV2.SetLabel(' ')
    gFrame.m_textSrMV3.SetLabel(' ')
    gFrame.m_textSrMV4.SetLabel(' ')
    gFrame.m_textSrPS1.SetLabel(' ')

    gFrame.m_textSr01.SetLabel('1')
    gFrame.m_textSr02.SetLabel('2')
    gFrame.m_textSr03.SetLabel('3')
    gFrame.m_textSr04.SetLabel('4')
    gFrame.m_textSr05.SetLabel('5')
    gFrame.m_textSr06.SetLabel('6')
    gFrame.m_textSr07.SetLabel('7')
    gFrame.m_textSr08.SetLabel('8')
    gFrame.m_textSr09.SetLabel('9')
    gFrame.m_textSr10.SetLabel('10')
    gFrame.m_textSr11.SetLabel('11')
    gFrame.m_textSr12.SetLabel('12')
    gFrame.m_textSr13.SetLabel('13')
    gFrame.m_textSr14.SetLabel('14')
    gFrame.m_textSr15.SetLabel('15')
    gFrame.m_textSr16.SetLabel('16')
    gFrame.m_textSr17.SetLabel('17')
    gFrame.m_textSr18.SetLabel('18')
    gFrame.m_textSr19.SetLabel(' ')
    gFrame.m_textSr20.SetLabel(' ')
    gFrame.m_textSr21.SetLabel(' ')
    gFrame.m_textSr22.SetLabel(' ')
    gFrame.m_textSr23.SetLabel(' ')
    gFrame.m_textSr24.SetLabel(' ')
    gFrame.m_textSr25.SetLabel(' ')
    gFrame.m_textSr26.SetLabel(' ')
    gFrame.m_textSr27.SetLabel(' ')
    gFrame.m_textSr28.SetLabel(' ')
    gFrame.m_textSr29.SetLabel(' ')
    gFrame.m_textSr30.SetLabel(' ')
    gFrame.m_textSr31.SetLabel(' ')
    gFrame.m_textSr32.SetLabel(' ')
    gFrame.m_textSr33.SetLabel(' ')
    gFrame.m_textSr34.SetLabel(' ')
    gFrame.m_textSr35.SetLabel(' ')
    gFrame.m_textSr36.SetLabel(' ')   
    #gFrame.m_textSrMV.SetFont(m_Large_Font)
    #gFrame.m_textSrMV.SetPoint(fontPointSz)               
def stationValveReportTxtLC36():
    fontPointSz=16

    gFrame.m_textSrMV1.SetLabel('MV1')
    gFrame.m_textSrMV2.SetLabel(' ')
    gFrame.m_textSrMV3.SetLabel(' ')
    gFrame.m_textSrMV4.SetLabel(' ')
    gFrame.m_textSrPS1.SetLabel(' ')

    gFrame.m_textSr01.SetLabel('1')
    gFrame.m_textSr02.SetLabel('2')
    gFrame.m_textSr03.SetLabel('3')
    gFrame.m_textSr04.SetLabel('4')
    gFrame.m_textSr05.SetLabel('5')
    gFrame.m_textSr06.SetLabel('6')
    gFrame.m_textSr07.SetLabel('7')
    gFrame.m_textSr08.SetLabel('8')
    gFrame.m_textSr09.SetLabel('9')
    gFrame.m_textSr10.SetLabel('10')
    gFrame.m_textSr11.SetLabel('11')
    gFrame.m_textSr12.SetLabel('12')
    gFrame.m_textSr13.SetLabel('13')
    gFrame.m_textSr14.SetLabel('14')
    gFrame.m_textSr15.SetLabel('15')
    gFrame.m_textSr16.SetLabel('16')
    gFrame.m_textSr17.SetLabel('17')
    gFrame.m_textSr18.SetLabel('18')
    gFrame.m_textSr19.SetLabel('19')
    gFrame.m_textSr20.SetLabel('20')
    gFrame.m_textSr21.SetLabel('21')
    gFrame.m_textSr22.SetLabel('22')
    gFrame.m_textSr23.SetLabel('23')
    gFrame.m_textSr24.SetLabel('24')
    gFrame.m_textSr25.SetLabel('25')
    gFrame.m_textSr26.SetLabel('26')
    gFrame.m_textSr27.SetLabel('27')
    gFrame.m_textSr28.SetLabel('28')
    gFrame.m_textSr29.SetLabel('29')
    gFrame.m_textSr30.SetLabel('30')
    gFrame.m_textSr31.SetLabel('31')
    gFrame.m_textSr32.SetLabel('32')
    gFrame.m_textSr33.SetLabel('33')
    gFrame.m_textSr34.SetLabel('34')
    gFrame.m_textSr35.SetLabel('35')
    gFrame.m_textSr36.SetLabel('36')   
    #gFrame.m_textSrMV.SetFont(m_Large_Font)
    #gFrame.m_textSrMV.SetPoint(fontPointSz)
    
def stationValveReportSet(stnValve,valveCurrent):
   # maps stnValve to a screen Valve
   #Generally
    dplyValveUsed=True
    #print ("stnValve="+str(stnValve))
    valveCurrent_mA=str(valveCurrent)+'mA'
    if   0==stnValve:
        gFrame.m_textSrMV1.SetLabel('MV1='+valveCurrent_mA)
    elif 1==stnValve:
        gFrame.m_textSr01.SetLabel('Stn'+str(stnValve)+'='+valveCurrent_mA)
    elif 2==stnValve:     
        gFrame.m_textSr02.SetLabel(str(stnValve)+'='+valveCurrent_mA)
    elif 3==stnValve:
        gFrame.m_textSr03.SetLabel(str(stnValve)+'='+valveCurrent_mA)             
    elif 4==stnValve:       
        gFrame.m_textSr04.SetLabel(str(stnValve)+'='+valveCurrent_mA)
    elif 5==stnValve:
        gFrame.m_textSr05.SetLabel(str(stnValve)+'='+valveCurrent_mA)            
    elif 6==stnValve: 
        gFrame.m_textSr06.SetLabel(str(stnValve)+'='+valveCurrent_mA)   
    elif 7==stnValve:
        gFrame.m_textSr07.SetLabel(str(stnValve)+'='+valveCurrent_mA)   
    elif 8==stnValve: 
        gFrame.m_textSr08.SetLabel(str(stnValve)+'='+valveCurrent_mA)   
    elif 9==stnValve:
        gFrame.m_textSr09.SetLabel(str(stnValve)+'='+valveCurrent_mA)   
    elif 10==stnValve:
        gFrame.m_textSr10.SetLabel(str(stnValve)+'='+valveCurrent_mA)   
    elif 11==stnValve:
        gFrame.m_textSr11.SetLabel(str(stnValve)+'='+valveCurrent_mA)    
    elif 12==stnValve:   
        gFrame.m_textSr12.SetLabel(str(stnValve)+'='+valveCurrent_mA)    
    elif 13==stnValve:   
        gFrame.m_textSr13.SetLabel(str(stnValve)+'='+valveCurrent_mA)    
    elif 14==stnValve: 
        gFrame.m_textSr14.SetLabel(str(stnValve)+'='+valveCurrent_mA)    
    elif 15==stnValve:   
        gFrame.m_textSr15.SetLabel(str(stnValve)+'='+valveCurrent_mA)    
    elif 16==stnValve:
        gFrame.m_textSr16.SetLabel(str(stnValve)+'='+valveCurrent_mA)    
    elif 17==stnValve:
        gFrame.m_textSr17.SetLabel(str(stnValve)+'='+valveCurrent_mA) 
    elif 18==stnValve:
        gFrame.m_textSr18.SetLabel(str(stnValve)+'='+valveCurrent_mA)    
    elif 19==stnValve:
        gFrame.m_textSr19.SetLabel(str(stnValve)+'='+valveCurrent_mA)   
    elif 20==stnValve: 
        gFrame.m_textSr20.SetLabel(str(stnValve)+'='+valveCurrent_mA)    
    elif 21==stnValve: 
        gFrame.m_textSr21.SetLabel(str(stnValve)+'='+valveCurrent_mA)    
    elif 22==stnValve:   
        gFrame.m_textSr22.SetLabel(str(stnValve)+'='+valveCurrent_mA)    
    elif 23==stnValve:  
        gFrame.m_textSr23.SetLabel(str(stnValve)+'='+valveCurrent_mA)             
    elif 24==stnValve:  
        gFrame.m_textSr24.SetLabel(str(stnValve)+'='+valveCurrent_mA)     
    elif 25==stnValve:
        gFrame.m_textSr25.SetLabel(str(stnValve)+'='+valveCurrent_mA)             
    elif 26==stnValve: 
        gFrame.m_textSr26.SetLabel(str(stnValve)+'='+valveCurrent_mA)
    elif 27==stnValve: 
        gFrame.m_textSr27.SetLabel(str(stnValve)+'='+valveCurrent_mA)
    elif 28==stnValve: 
        gFrame.m_textSr28.SetLabel(str(stnValve)+'='+valveCurrent_mA)
    elif 29==stnValve: 
        gFrame.m_textSr29.SetLabel(str(stnValve)+'='+valveCurrent_mA)
    elif 30==stnValve: 
        gFrame.m_textSr30.SetLabel(str(stnValve)+'='+valveCurrent_mA)
    elif 31==stnValve: 
        gFrame.m_textSr31.SetLabel(str(stnValve)+'='+valveCurrent_mA)
    elif 32==stnValve: 
        gFrame.m_textSr32.SetLabel(str(stnValve)+'='+valveCurrent_mA)
    elif 33==stnValve: 
        gFrame.m_textSr33.SetLabel(str(stnValve)+'='+valveCurrent_mA)
    elif 34==stnValve: 
        gFrame.m_textSr34.SetLabel(str(stnValve)+'='+valveCurrent_mA)
    elif 35==stnValve: 
        gFrame.m_textSr35.SetLabel(str(stnValve)+'='+valveCurrent_mA)
    elif 36==stnValve: 
        gFrame.m_textSr36.SetLabel(str(stnValve)+'='+valveCurrent_mA)
    elif 101==stnValve:
        #gFrame.m_textSrMV1.SetLabel('MV1='+valveCurrent_mA)
        gFrame.m_textSr10.SetLabel('MV1='+valveCurrent_mA)
    elif 102==stnValve:
        #gFrame.m_textSrMV2.SetLabel('MV2='+valveCurrent_mA)
        gFrame.m_textSr11.SetLabel('MV3='+valveCurrent_mA)
    elif 103==stnValve:
        #gFrame.m_textSrMV3.SetLabel('MV3='+valveCurrent_mA)
        gFrame.m_textSr12.SetLabel('MV3='+valveCurrent_mA)
    elif 104==stnValve:
        #gFrame.m_textSrMV4.SetLabel('MV4='+valveCurrent_mA)
        gFrame.m_textSr13.SetLabel('MV4='+valveCurrent_mA)
    elif 201==stnValve:
        #gFrame.m_textSrPS1.SetLabel('PS1='+valveCurrent_mA)
        gFrame.m_textSr19.SetLabel('PS1='+valveCurrent_mA)
    else :
        dplyValveUsed=False
        print("stationValveReportSet "+str(stnValve)+"not mapped")


def keyReportPanelSet(keybtn):
    global gKeyPressed_Count
    keyBtnPressed=True
    #print ("keybtn="+str(keybtn))
    if   1==keybtn:
        if not gFrame.m_checkBoxKr01.IsChecked(): gKeyPressed_Count+=1    
        gFrame.m_checkBoxKr01.SetValue(True)
    elif 2==keybtn:
        if not gFrame.m_checkBoxKr02.IsChecked(): gKeyPressed_Count+=1            
        gFrame.m_checkBoxKr02.SetValue(True)
    elif 3==keybtn:
        if not gFrame.m_checkBoxKr03.IsChecked(): gKeyPressed_Count+=1    
        gFrame.m_checkBoxKr03.SetValue(True)             
    elif 4==keybtn:
        if not gFrame.m_checkBoxKr04.IsChecked(): gKeyPressed_Count+=1            
        gFrame.m_checkBoxKr04.SetValue(True)
    elif 5==keybtn:
        if not gFrame.m_checkBoxKr05.IsChecked(): gKeyPressed_Count+=1    
        gFrame.m_checkBoxKr05.SetValue(True)            
    elif 6==keybtn:
        if not gFrame.m_checkBoxKr06.IsChecked(): gKeyPressed_Count+=1    
        gFrame.m_checkBoxKr06.SetValue(True)   
    elif 7==keybtn:
        if not gFrame.m_checkBoxKr07.IsChecked(): gKeyPressed_Count+=1    
        gFrame.m_checkBoxKr07.SetValue(True)   
    elif 8==keybtn:
        if not gFrame.m_checkBoxKr08.IsChecked(): gKeyPressed_Count+=1    
        gFrame.m_checkBoxKr08.SetValue(True)   
    elif 9==keybtn:
        if not gFrame.m_checkBoxKr09.IsChecked(): gKeyPressed_Count+=1    
        gFrame.m_checkBoxKr09.SetValue(True)   
    elif 10==keybtn:
        if not gFrame.m_checkBoxKr10.IsChecked(): gKeyPressed_Count+=1    
        gFrame.m_checkBoxKr10.SetValue(True)   
    elif 11==keybtn:
        if not gFrame.m_checkBoxKr11.IsChecked(): gKeyPressed_Count+=1    
        gFrame.m_checkBoxKr11.SetValue(True)    
    elif 12==keybtn:
        if not gFrame.m_checkBoxKr12.IsChecked(): gKeyPressed_Count+=1    
        gFrame.m_checkBoxKr12.SetValue(True)    
    elif 13==keybtn:
        if not gFrame.m_checkBoxKr13.IsChecked(): gKeyPressed_Count+=1    
        gFrame.m_checkBoxKr13.SetValue(True)    
    elif 14==keybtn:
        if not gFrame.m_checkBoxKr14.IsChecked(): gKeyPressed_Count+=1    
        gFrame.m_checkBoxKr14.SetValue(True)    
    elif 15==keybtn:
        if not gFrame.m_checkBoxKr15.IsChecked(): gKeyPressed_Count+=1    
        gFrame.m_checkBoxKr15.SetValue(True)    
    elif 16==keybtn:
        if not gFrame.m_checkBoxKr16.IsChecked(): gKeyPressed_Count+=1    
        gFrame.m_checkBoxKr16.SetValue(True)    
    #elif 17==keybtn:
    #    if not gFrame.m_checkBoxKr03.IsChecked(): gKeyPressed_Count+=1    
    #     gFrame.m_checkBoxKr17.SetValue(True)    
    elif 18==keybtn:
        if not gFrame.m_checkBoxKr18.IsChecked(): gKeyPressed_Count+=1    
        gFrame.m_checkBoxKr18.SetValue(True)    
    #elif 19==keybtn:
    #    if not gFrame.m_checkBoxKr03.IsChecked(): gKeyPressed_Count+=1    
    #     gFrame.m_checkBoxKr19.SetValue(True)    
    elif 20==keybtn:
        if not gFrame.m_checkBoxKr20.IsChecked(): gKeyPressed_Count+=1    
        gFrame.m_checkBoxKr20.SetValue(True)    
    elif 21==keybtn:
        if not gFrame.m_checkBoxKr21.IsChecked(): gKeyPressed_Count+=1    
        gFrame.m_checkBoxKr21.SetValue(True)    
    elif 22==keybtn:
        if not gFrame.m_checkBoxKr22.IsChecked(): gKeyPressed_Count+=1    
        gFrame.m_checkBoxKr22.SetValue(True)    
    elif 23==keybtn:
        if not gFrame.m_checkBoxKr23.IsChecked(): gKeyPressed_Count+=1    
        gFrame.m_checkBoxKr23.SetValue(True)             
    elif 24==keybtn:
        if not gFrame.m_checkBoxKr24.IsChecked(): gKeyPressed_Count+=1    
        gFrame.m_checkBoxKr24.SetValue(True)     
    elif 25==keybtn:
        if not gFrame.m_checkBoxKr25.IsChecked(): gKeyPressed_Count+=1    
        gFrame.m_checkBoxKr25.SetValue(True)             
    elif 26==keybtn:
        if not gFrame.m_checkBoxKr26.IsChecked(): gKeyPressed_Count+=1    
        gFrame.m_checkBoxKr26.SetValue(True)                
    else :
        keyBtnPressed=False
    #    print("unknown key "+str(keybtn))
    #if True==keyBtnPressed :
    #gKeyPressed_Count+=1                  
    gFrame.m_gaugeKeyReport.SetValue(gKeyPressed_Count) 
    #gFrame.m_gaugeKeyReport.SetColor - no way to do this
        
def keyReportPanelFalse():
    global gKeyPressed_Count
    gKeyPressed_Count=0
    gFrame.m_gaugeKeyReport.SetValue(gKeyPressed_Count) 
    gFrame.m_checkBoxKr01.SetValue(False)    
    gFrame.m_checkBoxKr02.SetValue(False)
    gFrame.m_checkBoxKr03.SetValue(False)             
    gFrame.m_checkBoxKr04.SetValue(False)
    gFrame.m_checkBoxKr05.SetValue(False)            
    gFrame.m_checkBoxKr06.SetValue(False)   
    gFrame.m_checkBoxKr07.SetValue(False)
    gFrame.m_checkBoxKr08.SetValue(False)   
    gFrame.m_checkBoxKr09.SetValue(False)   
    gFrame.m_checkBoxKr10.SetValue(False)   
    gFrame.m_checkBoxKr11.SetValue(False)    
    gFrame.m_checkBoxKr12.SetValue(False)    
    gFrame.m_checkBoxKr13.SetValue(False)    
    gFrame.m_checkBoxKr14.SetValue(False)    
    gFrame.m_checkBoxKr15.SetValue(False)    
    gFrame.m_checkBoxKr16.SetValue(False)    
    #gFrame.m_checkBoxKr17.SetValue(False)    
    gFrame.m_checkBoxKr18.SetValue(False)    
    #gFrame.m_checkBoxKr19.SetValue(False)    
    gFrame.m_checkBoxKr20.SetValue(False)    
    gFrame.m_checkBoxKr21.SetValue(False)    
    gFrame.m_checkBoxKr22.SetValue(False)    
    gFrame.m_checkBoxKr23.SetValue(False)             
    gFrame.m_checkBoxKr24.SetValue(False)     
    gFrame.m_checkBoxKr25.SetValue(False)               
    gFrame.m_checkBoxKr26.SetValue(False)  

     
def doKeyboardTest():
    tstResult = HSUCCESS
    global gKeyPressed_Count
    global gKeyReportComplete
    gFrame.m_textCtrlKeyReport11.Show(True)
    gFrame.m_textCtrlKeyReport11.AppendText("Test at least "+str(gKeyboardRead)+" keys.\n\rPressing Sound key ends test")
    Notebook1CurrentPg = gFrame.m_notebook1.GetSelection()
    gFrame.m_notebook1.ChangeSelection(uiTabKeyReport_Num)

    # Do keyboard tests
    gKeyReportComplete=0
    UiKeyPause_NUM = 21
    UserSoundKeyState =  gFrame.m_checkBoxKr21.GetValue()
    #logging.debug('doKeyBoardTest Start '+str(UserSoundKeyState))
    keyReportPanelFalse()
    while True : 
        #time.sleep(1)
        if 0<gKeyReportComplete :
            logging.debug('KeyReportComplete Evt: '+str(gKeyReportComplete)+', KeysTested:'+str(gKeyPressed_Count))
            # if 12 start again
            break
        keybtn_str=hym.getButton()
        if keybtn_str :
            keybtn_str_len = len(keybtn_str)
            #logging.debug("keybtn["+str(keybtn_str_len)+"]="+keybtn_str)
            keybtn = int(keybtn_str[:-1])
            keyReportPanelSet(keybtn)
            if gKeyboardTestBeepOn :
                hym.setBeep()
            if keybtn == UiKeyPause_NUM :
                break
        UserSoundKeyState = gFrame.m_checkBoxKr21.GetValue()
        #logging.debug("doKey "+str(UserSoundKeyState))
        if True == UserSoundKeyState :
            break

    uiStatus = UiConsoleStatus_PASS
    msgUser = "keyboard Test"+str(gKeyboardRead)+"keys: Passed "+str(gKeyPressed_Count)
    if gKeyboardRead  > gKeyPressed_Count :
        uiStatus = UiConsoleStatus_FAIL
        tstResult = HFAIL
        hym.setBeep()
        msgUser = "keyboard Test"+str(gKeyboardRead)+"keys: Failed "+str(gKeyboardRead -gKeyPressed_Count)
        gFrame.uiConsole(msgUser,uiStatus)
        gFrame.UserDialogOK( msgUser, caption = 'Keyboard Finished') 
    else : 
        gFrame.uiConsole(msgUser,uiStatus) 

    gFrame.m_notebook1.ChangeSelection(Notebook1CurrentPg)
    return tstResult
    
def doStationValveTest() :
    tstResult = HSUCCESS
    # Test Station Valves by turning on, expecting a defined load and measauring current
    # LC18 this is 18Valves & MV
    # LC36 this is 36Valves & MV
    #For LC18 there is MV+ stations 1-19, driven by Drivers 1-24,
    #For LC36 there is MV+ Stations 1-36, driven through Drivers 1-40
    # all other valves are sequential 
    ValveStnMax_mA =  gStationtestMax_ma
    ValveStnMin_mA =  gStationtestMin_ma
    ValveMvMax_mA  =  gMvtestMax_ma
    ValveMvMin_mA  =  gMvtestMin_ma
    ValveAdcZero   = 1024
    #Mult for converting ADC range to mA -moved to embedded code
    #ValveAdcMultLC = 2.31  #same for LC18 & 36
    ValveAdcMult2W2G = 4.60  #same for LC18 & 36   
    # Current adjustment done at PIC32 ~ LC 2.31, Pro3 adapter is 4.48, Pro3 2-wire is 1.12, LC36 is 2.31
    gFrame.uiConsole("Station Test started. Range Stations:"+str(ValveStnMin_mA)+"-"+str(ValveStnMax_mA)+"mA MV:"+str(ValveMvMin_mA)+"-"+str(ValveMvMax_mA)+"mA",UiConsoleStatus_PASS)

    station_NUM =0 #range is 0,1-18
    # Translate station valve to driver 
    #  LC18 station valves are 0=MV, 1-->18. driver is 1-24
    #                MV 1 2 3 4 5 6 7    8  9 10 11 12 13 14 15    16 17 18 
    valveLookupLC18 =[7,1,2,3,4,5,6,9,  10,11,12,13,14,17,18,19,   20,21,22]
    # User display mapping - sequence to Display Valve - StationValveReportSet()
    dsplyLookupLC18 =[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18] 

    #
    #LC HW has 40 bit (5*8) shift register assigned as follows - shift register to station
    #  0 based        0 1 2 3 4 5 6 7   8  9 10 11 12 13 14 15   16 17 18 19 20 21 22 23   24 25 26 27 28 29 30 31   32 33 34 35 36 37 38 39
    #  1 based        1 2 3 4 5 6 7 8   9 10 11 12 13 14 15 16   17 18 19 20 21 22 23 24   25 26 27 28 29 30 31 32   33 34 35 36 37 38 39 40
    #                 1 2 3 4 5 6 M 7   8 9  10 11 12 13 14 15   16 17 18 36 35 34 33 32   -- 31 30 29 28 27 26 25   -- 24 23 22 21 20 19 --
    # LC36 station valves are managed as  0=MV, 1-->36, (test interface driver is 1-40, one based )
    # This table translates station number to ones based bit position in shift register
    #                MV 1 2 3 4 5 6 7  8  9 10 11 12 13 14 15   16 17 18 19 20 21 22 23   24 25 26 27 28 29 30 31   32 33 34 35 36 
    valveLookupLC36 =[7,  1,2,3,4,5,6,8, 9,10,11,12,13,14,15,16,  17,18,19,39,38,37,36,35,  34,32,31,30,29,28,27,26,  24,23,22,21,20]
    # User display mapping - sequence to Display Valve - StationValveReportSet()
    dsplyLookupLC36 =[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36] 

	# 
    #PRO3 "2-Wire Direct Connect" board has 5 direct outputs - 4MV and 1PS
    # The MSP430 supports current avg. 
    #For Rev1bpard the MV1-4 label is correct, LEDs turning on are reversed
    #                    MV1 MV2 MV3 MV4 PS Stn001
    valveLookupPRO_2WG2 =[  1,  2,  3,  4,  5, 6]
    # User display mapping - sequence to Display Valve - StationValveReportSet()
    dsplyLookupPRO_2WG2 =[101,102,103,104,201, 1]  

    if HwModelLC18 == hwModelEqu :
        valveLookup = valveLookupLC18
        dsplyLookupPtr = dsplyLookupLC18
        valveTot_Num =19
    elif HwModelLC36 == hwModelEqu :
        valveLookup = valveLookupLC36
        dsplyLookupPtr = dsplyLookupLC36
        valveTot_Num =37
    elif HwModel2W2G== hwModelEqu :
        valveLookup = valveLookupPRO_2WG2
        dsplyLookupPtr = dsplyLookupPRO_2WG2
        valveTot_Num =6
    else :
        #assume PRO3 - need to configure valveLookip
        gFrame.uiConsole(" Station Tests for "+dut_specification+" not supported",2)      
        return 1 #imply at least one failure
        
    gFrame.uiConsole(" Selected "+str(valveTot_Num)+" valves for "+dut_specification,2)  
    valveTestsFailed=0
    Notebook1CurrentPg = gFrame.m_notebook1.GetSelection()
    gFrame.m_notebook1.ChangeSelection(uiTabStationReport_Num)
    stationLastTested =0
    
    for station_NUM in range(0,valveTot_Num) :
        stationLastTested = station_NUM
        valveDrv = valveLookup[station_NUM]
        stationTestFailed = False
        #print("valvePhy:drv="+str(station_NUM)+":"+str(valveDrv))
        hym.setValveOn(valveDrv)
        
        # Get current - method depends on target
        # - for H2O variants 7.10 , there are two sources of current
        #          for direct valves  - getCurrentAvgAdc()
        #         - for 2W2G valves - getCurrent2w2g()
        # - for LC36 can use hym.getCurrentAvgAdc(), that uses the LC36 MSP430 to do the avg - only supported >7.8
        # - for LC18 7.8 or earlier use hym.getCurrentSampleAdc()
        # - for LC18 post 7.10 talk with Glenn/Greg as to if can use hym.getCurrentAvgAdc(), where the MSP430 calculates avg AC current  
        # There is a multiplier for the ADC value, that is the same from LC18/36 - different MULT for Pro3s

        #gVerDutMjInt expected to be 7 if get here
        valveCurrentAdc =0
        if  (gVerDutMnInt >8 and (HwModelLC18 != hwModelEqu) ) : #and effecitvely  (HwModelLC36 == hwModelEqu ) as blocked by .cfg  StationTestAutoOff=1
        #if  (gVerDutMnInt >8 ) : #and effecitvely  (HwModelLC36 == hwModelEqu ) as blocked by .cfg  StationTestAutoOff=1

            if ((6 == valveDrv) and (HwModel2W2G== hwModelEqu)) :
                valveCurrentAdc = hym.getCurrent2w2g()
                logging.debug( "st2w2gA="+str(valveCurrentAdc)+" mA",2)
            else :
                valveCurrentAdc = hym.getCurrentAvgAdc()
                logging.debug("getCurrentAvgAdc="+str(valveCurrentAdc)+" mA")

            if not valveCurrentAdc :
                valveCurrent_mA = 0
                stationTestFailed = True
                #break
            else :
                #valveCurrent_mA = int(ValveAdcMult2W2G   *valveCurrentAdc)
				#valveCurrent_mA = int(ValveAdcMultLC*valveCurrentAdc)
                valveCurrent_mA = int(valveCurrentAdc)
        
        else : # gVerDutMnInt <9 which is only HwModelLC18 
            hym.getCurrentSampleAdc() #first reading is previous get current, throw away
            valveAdcMin = ValveAdcZero
            valveAdcMax = ValveAdcZero
            valveCurrent = 0
            CurrentLpTot = 20 # number of times to do ADC
    
            #if gVerDutMnInt >8  :          
            #    valveCurrent_mA = int(ValveAdcMultLC*hym.getCurrentAvgAdc())
            #if True : #else : #gVerDutMnInt <=8
            for currentLp in range(CurrentLpTot) :
                #time.sleep(1)# for testing 
                #Current is offset from 1024 on 60HZ/16.66mS cycle, look for min and max
                valveCurrentAdc = hym.getCurrentSampleAdc()
                if not valveCurrentAdc :
                    stationTestFailed = True
                    break
                if valveCurrentAdc >= ValveAdcZero :
                    if valveCurrentAdc> valveAdcMax:
                        valveAdcMax = valveCurrentAdc
                else:
                    if valveCurrentAdc< valveAdcMin:
                        valveAdcMin = valveCurrentAdc
            
            if 0 == valveAdcMin :
                #special case of not reporting current
                valveCurrent_mA = 0
            else :      
                valveCurrent_mA = int(valveAdcMax-valveAdcMin)
            
            logging.debug( "getCurrentSampleAdc="+str(valveCurrent_mA)+" mA",2)
            if stationTestFailed :
                gFrame.uiConsole("stationTest["+str(station_NUM)+"]: Card appears to be powered off - not responding",2)       
                break;
                
        #valveCurrent_mA = valveCurrent_mATot/CurrentLpTot
        #print("valveCurrent="+str(valveCurrent_mA)+" "+str(valveAdcMax)+":"+str(valveAdcMin))
        if valveCurrent_mA <20 :
            valveCurrent_mA=0
        
        if 0 == station_NUM :
            if valveCurrent_mA >= ValveMvMin_mA and valveCurrent_mA <= ValveMvMax_mA:
                #good else fail
                #print("Test=PASS, MV,mA="+str(valveCurrent_mA))
                stationValveReportSet(dsplyLookupPtr[station_NUM],valveCurrent_mA)
            else :
                infoStr = "Test=FAIL, MV,mA="+str(valveCurrent_mA)+", range "+str(ValveMvMin_mA)+":"+str(ValveMvMax_mA)
                gFrame.uiConsole(infoStr,UiConsoleStatus_FAIL)
                stationValveReportSet(dsplyLookupPtr[station_NUM],valveCurrent_mA)
                valveTestsFailed+=1
        else :
            if valveCurrent_mA  >= ValveStnMin_mA and valveCurrent_mA <= ValveStnMax_mA:
                #print("Test=PASS, station="+str(station_NUM)+",mA="+str(valveCurrent_mA))
                stationValveReportSet(dsplyLookupPtr[station_NUM],valveCurrent_mA)
            else :
                infoStr = "Test=FAIL, StationV="+str(station_NUM)+",mA="+str(valveCurrent_mA)+", range "+str(ValveStnMin_mA)+":"+str(ValveStnMax_mA)
                gFrame.uiConsole(infoStr, UiConsoleStatus_FAIL)
                stationValveReportSet(dsplyLookupPtr[station_NUM],valveCurrent_mA)
                valveTestsFailed+=1

    hym.setValvesOff()
    time.sleep(1)
    if valveTestsFailed : gFrame.UserDialogOK( 'Fix hardware and retest', caption = 'Valve Test failed')

    gFrame.m_notebook1.ChangeSelection(Notebook1CurrentPg)
    #if (stationTestFailed) or (valveTestsFailed != 0) :
    #   msgUser = "Station Valve tests FAILED="+str(valveTestsFailed)
    #    gFrame.UserDialogOK( msgUser, caption = 'Station Valve Test Finished')
    #    return
    gFrame.uiConsole("Station Test end, failed["+str(valveTestsFailed)+']')
       
    if 0== valveTestsFailed :
        testStatus =UiConsoleStatus_PASS
    else :
        testStatus = UiConsoleStatus_FAIL
        tstResult = HFAIL

    gFrame.uiConsole("Station Test Complete: Stations Passed="+str(valveTot_Num-valveTestsFailed)+" Failed="+str(valveTestsFailed)+" out of "+str(valveTot_Num) +" Valves",testStatus)
    
    return tstResult

def doStationTestWithLogging():
    tstResult = HSUCCESS
    stationValveReportInit()
    if True == gStationTestAuto:
        tstResult = doStationValveTest()
    
    else :
        msgUser = "Run Station Valve tests manually. If ALL PASS press YES, else if any FAIL No"
        userDialog = gFrame.UserDialogYesNo( msgUser, caption = 'Manual Station Valve test pass')
        userAction="FAILED"
        if userDialog :
            userAction="PASSED"
        gFrame.uiConsole("Operator records Station Valve Test "+str(userAction))
        if False == userAction :
            tstResult = HFAIL
            
    return tstResult

def cardEepromTst():
    EepromTst = hym.testEeprom()
    #print("EepromTst="+EepromTst)
    uiStatus =UiConsoleStatus_PASS
    tstResult = HSUCCESS
    if "PASS\0" != EepromTst :
        msgUser=" EEPROM test failed."
        uiStatus = UiConsoleStatus_FAIL
        tstResult =HFAIL
        gFrame.ShowMessageUser(msgUser)
    else:
        msgUser=" EEPROM test passed."
    gFrame.uiConsole( msgUser,uiStatus)
    return tstResult
     
def cardRtcTst():
    rtcTst = hym.testRtc()
    #print("rtcTst="+rtcTst)
    uiStatus =UiConsoleStatus_PASS
    tstResult = HSUCCESS
    if "PASS\0" != rtcTst :
        msgUser=" RTC test failed."
        uiStatus = UiConsoleStatus_FAIL
        gFrame.ShowMessageUser(msgUser)
    else:
        msgUser=" RTC test passed."
    gFrame.uiConsole( msgUser,uiStatus)
    return tstResult
    
def cardSlotsTst():
    tstResult = HSUCCESS
    uiStatus = UiConsoleStatus_PASS
    for slot_num in range(1,4):
        rspTst = hym.getSlot(slot_num)
        #print("slotTst["+str(slot_num)+"]="+rspTst)
        if "0\0" == rspTst :
            msgUser=" SLOT="+str(slot_num)+" test failed."
            uiStatus = UiConsoleStatus_FAIL
            tstResult = HFAIL
            gFrame.uiConsole( msgUser,uiStatus)
            gFrame.ShowMessageUser(msgUser)
    if HSUCCESS==tstResult :
        msgUser=" SLOTs test passed."
        gFrame.uiConsole( msgUser,uiStatus)
    return tstResult

def cardRainSwitchTst():
    #Check for open/closed RainSwitch test
    tstResult = HSUCCESS

    msgUser=" Remove Rain Switch. Press OK when out"
    uiStatus = UiConsoleStatus_FAIL
    #gFrame.ShowMessageUser(msgUser)
    gFrame.UserDialogAction(msgUser)
    rspTst = hym.getRainSwitch()
    #print('rainSw="'+rspTst+'"')
    uiStatus =UiConsoleStatus_PASS
    if "1" == rspTst[0] :
        msgUser=" Return Rain Switch. Press OK when seated. (nextUp: station test, then flow test)"
        #gFrame.ShowMessageUser(msgUser)
        gFrame.UserDialogAction(msgUser)
        rspTst = hym.getRainSwitch()
        if "0" == rspTst[0] :
            msgUser=" RainSwitch test passed."
        else:
            msgUser=" RainSwitch test closed failed."
            uiStatus = UiConsoleStatus_FAIL
            tstResult = HFAIL
            gFrame.ShowMessageUser(msgUser)
    else:
        msgUser=" RainSwitch Open Failed."
        tstResult = HFAIL
        gFrame.ShowMessageUser(msgUser)
        uiStatus = UiConsoleStatus_FAIL
    gFrame.uiConsole( msgUser,uiStatus)
    return tstResult

def cardFlowTst():
    tstResult = HSUCCESS
    flowPeriodSec=2
    startFlow =[0,0,0,0]
    endFlow =[0,0,0,0]
    calcFlow =[0,0,0,0]
    
    #flow ranges
    flowRange1 =[13,19]
    #flowRange4 =[26,38, 13,19,  7,9, 3,5]
    #flowRange4 =[30,50, 15,30,  7,15, 3,7] #needs to be ranged to traceable units
    flowRange4 =[14,17,  6,9,  3,5, 1,3] #[[GPMlow,GPMhigh],[]]  needs to be ranged to traceable units
    
    #setup for standard 1flow
    flowTestMax=1
    flowRange = flowRange1
    flowConst =4
    #4Flow test for _2W2G
    if HwModel2W2G== hwModelEqu :
        flowTestMax=4
        flowRange=flowRange4
        flowConst =5.7188
          
    gFrame.uiConsole(" Flow test("+str(flowTestMax)+") started - waiting "+str(flowPeriodSec)+"seconds",2)
    #TODO if PRO3 2-wire G2/baseline then test for 4 flows.
    for flow_NUM in range(0,flowTestMax) :   
        startFlow[flow_NUM ] = hym.getFlow(flow_NUM)
        
    time.sleep(flowPeriodSec)
    for flow_NUM in range(0,flowTestMax) :  
        endFlow[flow_NUM]= hym.getFlow(flow_NUM)
    #should have 2*120 transitions - representing 16GPM +/- 20%
    for flow_NUM in range(0,flowTestMax) :  
        #use a range that doesn't loolk
        calcFlow=(endFlow[flow_NUM ]-startFlow[flow_NUM ])/(flowPeriodSec*flowConst)
        #calcFlow=(endFlow[flow_NUM ]-startFlow[flow_NUM ])/(4*flowPeriodSec)
        flowMin=flowRange[2*flow_NUM]
        flowMax=flowRange[(2*flow_NUM)+1]
        #print("flowTst="+str(calcFlow)+'='+str(endFlow)+'-'+str(startFlow))
        uiStatus =UiConsoleStatus_PASS
        # Need a comparision of being with +-20% of 16GPM?
        if flowMax < calcFlow or flowMin> calcFlow:
            msgUser=" Flow test"+str(flow_NUM+1)+" failed="+str("{:5.1f}".format(calcFlow))+" Expected range="+str(flowMin)+":"+str(flowMax ) 
            #msgUser=" Flow test"+str(flow_NUM+1)+" failed="+str(calcFlow)+" Expected range="+str(flowMin)+":"+str(flowMax ) 
            uiStatus = UiConsoleStatus_FAIL
            tstResult = HFAIL
            gFrame.ShowMessageUser(msgUser)
        else:
            msgUser=" Flow test"+str(flow_NUM+1)+" passed="+str("{:5.1f}".format(calcFlow))+" GPM"
            #msgUser=" Flow test"+str(flow_NUM+1)+" passed="+str(calcFlow)+" GPM"
        gFrame.uiConsole( msgUser,uiStatus)
        
    return tstResult

def cardBeepTst():
    uiStatus =UiConsoleStatus_PASS
    tstResult = HSUCCESS
    hym.setBeep()
    if gFrame.UserDialogYesNo("Did it beep"):
        msgUser= " Beep test passed."
    else:
        msgUser=" Beep test failed."
        uiStatus = UiConsoleStatus_FAIL
        tstResult = HFAIL
    gFrame.uiConsole( msgUser,uiStatus)
    return tstResult
    

def cardLedsTst():
    uiStatus =UiConsoleStatus_PASS
    tstResult = HSUCCESS
    hym.setLeds(1)
    if gFrame.UserDialogYesNo("Are All LEDs ON. Yes passes. No Fails"):
        msgUser=" LEDS test passed."
    else:
        msgUser=" LEDS test failed."
        uiStatus = UiConsoleStatus_FAIL
        tstResult = HFAIL
    gFrame.uiConsole( msgUser,uiStatus)
    hym.setLeds(0)
    return tstResult

def configureSystem():
    gFrame.uiConsole( "Configure",2)
    writeContext()    
    readContext()
    respHwConfig=hym.getHwConfig()
    gFrame.uiConsole("HwConfig"+str(respHwConfig),2)
    setDefaultEeprom()

def allSystemTest():
    # this runs all the tests - configured through the globals
    tstResults = 0
    if gControlBoardTest :
        tstResults =  cardEepromTst() # Test Controller Board EEPROM    
        tstResults |= cardRtcTst() # Test Controller Board  
    if gStationKeysTest :        
        tstResults |= cardSlotsTst()  # Read & validate Slots/Keys 
        
    if gLedsTest :
    # LEDs test - to test physical LEDs 
    #OR for cmd to prove it can control the termninal board
        tstResults |= cardLedsTst()   
    if gBeepTest :
    # Test Buzzer on Control Board
        tstResults |= cardBeepTst()
    #
    # Get all Buttons - A beep is a configuration parameter
    if  not 0==gKeyboardRead :
        tstResults |= doKeyboardTest()
        
    if gRainswitchTest :
        tstResults |= cardRainSwitchTst()
    
    if gValveTest :
    #Validate all stations using mA passed
    #print("allSystemTest- "+str(tstResults))
        tstResults |= doStationTestWithLogging()
        #print("allSystemTest+ "+str(tstResults))

    if gFlowTest :
        tstResults |= cardFlowTst()# Check Flow
    
    return tstResults




def readContext():
    # Company Name, Code1, Code2
    #hym.getFactoryConfig()
    #localCustFactoryConfig = bytearray(50)
    #gSerHandle.reset_input_buffer()
    #time.sleep(.1) # To allow buffer to clear
    try:  #production
    #if 1: #debug
        snLp=0
        #bug serial number not always returned after write.
        while True:
            gCustSn = hym.getSn()
            if 7<len(gCustSn) : #
                break  # found good serial num
            #print("eCR received bad Sn sz "+str(len(gCustSn)))
            snLp+=1
            if 4<snLp :
                break
        
        fc_str=hym.getFactoryConfig()
        #print('ConfigureRead:',str(type(fc_str)))
        #if fc_str is None :
        if not fc_str :
            gFrame.uiConsole("Read failed, retry or reset")
        else:
            #print('fc_strSize:',fc_str.__sizeof__()) #60
            #print(fc_str)
            if ''==fc_str :
                gFrame.uiConsole("Read failed, retry or reset")
            else :
                pack800N=fc_str[:10]
                lCode1=fc_str[10:11].encode('utf-8')
                lCode2=fc_str[11:12].encode('utf-8')
                lCn=fc_str[12:36]
        
                #for character in fc_str:
                #print('n '+pack800N)
                #print("read type lCode "+str(type(lCode1)))
                #print(binascii.hexlify(lCode1))
                #print(binascii.hexlify(lCode2))
                #print('cn '+CompanyName)
                #Update visibility
                gFrame.m_confPcCode1.SetLabelText(binascii.hexlify(lCode1))
                gFrame.m_confPcCode2.SetLabelText(binascii.hexlify(lCode2))
                N800display ='1-'+pack800N[:3]+'-'+pack800N[3:6]+'-'+pack800N[6:]
                gFrame.m_conf800Num.SetLabelText(N800display)
                gFrame.m_confCompanyName.SetLabelText(lCn)
                gFrame.uiConsole("card: "+str(gCustSn)+"\n  Code1:"+str(ord(lCode1))+"\n  Code2:"+str(ord(lCode2))+'\n  800Number:'+N800display +'\n  Company Name:"'+ lCn+'"',
                             2)
                #gFrame.uiConsole("Writing("+str(gCustSn)+") Code1="+lCode1+"  Code2="+lCode2 +'  800Number='+pack800N +'  Company Name="'+ lCn+'"', 2)

        #Mywin.updateGuiConfigurationParm()    
    except : #(AttributeError,NoneType) :
    #else:
        msgExcept="Please power cycle target. igrc:GET READ "+str(sys.exc_info()[0])
        print(msgExcept)
        gFrame.ShowMessageUser(msgExcept)
        os._exit(1)

def writeContext():
    #global gCustSn
    #CustSn = hym.getSn()
    #if CustSn!=gCustSn:
    #    oldCustSn = gCustSn
    #    gCustSn=CustSn
    #    gFrame.uiConsole('New card ('+gCustSn+' Was '+oldCustSn)
    lCode1=gFrame.m_confPcCode1.GetValue()
    lCode2=gFrame.m_confPcCode2.GetValue()
    lCn   =gFrame.m_confCompanyName.GetValue()
    l800N =gFrame.m_conf800Num.GetValue()
    #remove punctuation
    pack800N=l800N[2:5]+l800N[6:9]+l800N[10:14]
    #factoryConfig=pack800N+bytes(lCode1)+bytes(lCode2)+lCn
    factoryConfig=bytearray()
    factoryConfig.extend(pack800N.encode('ascii')+struct.pack('BB',int(lCode1),int(lCode2))+lCn.encode('ascii')+b'\0')
    #print(binascii.hexlify(factoryConfig))
    resp=hym.setFactoryConfig(factoryConfig)
    logging.info('ConfigureWrite '+str(resp))
    if resp is None:
        msgUser="Written("+str(gCustSn)+")\n Code1="+lCode1+"\n  Code2="+lCode2 +'\n  800Number='+l800N +'\n  Company Name="'+ lCn+'"'
        gFrame.uiConsole( msgUser,2)
    else :
        msgUser="Write failed:"+resp
        gFrame.uiConsole(msgUser,1)

    gFrame.ShowMessageUser(msgUser)

def checkDutPresent():
    # Loop for a number of timeouts, so that more likely to connect if wtSysTest and Card started together
    attnLp=1
    while (attnLp<5):
        gFrame.uiConsole('Attempting connection '+str(attnLp)+' to card', status=2) 
        respAttn=hym.hyAttn()
        logging.debug('checkDutPresent:'+str(respAttn))
        if [] == respAttn :
            break
        attnLp += 1
    timeoutCheck(respAttn,'Please reset DUT, timeout on startup "Hello".\n' +uiComMsg)


def  setDefaultEeprom() :
    hym.setEepromDefault()
    msgUser="card: "+str(gCustSn)+" EEPROM defaulted and card restarted."
    gFrame.uiConsole( msgUser,2)
    msgInstructions='Please power cycle target (or verify power cylces). Press OK after LCD shows "EEPROM reset to defaults" '
    gFrame.ShowMessageUser(msgInstructions)
    time.sleep(2)
    #checkDutPresent()
    cardSnCheck()
    gFrame.uiConsole('Connected to card', status=2) 

def inUi_thread():
    " A thread to run the actions from"
    #print 'Queue-started '
    #while not inUi_queue.empty():
    global gCustSn
    
    while True:
        parm1=inUi_queue.get()
        logging.info('Queue-evt-received %d' % parm1)
#INQ_GET_CONTROLLER_STATUS_CMD = 1
#INQ_GET_TERMINAL_STATUS_CMD =   2
#INQ_GET_MODEM_STATUS_CMD =      3
#INQ_SET_CONTROLLER_STATUS_CMD = 4
#INQ_SET_SYSTEM_STATUS_CMD =     5 
#INQ_SET_1800_NUMBER_CMD =       6 
#INQ_GET_1800_NUMBER_CMD =       7 
#INQ_DO_VALVE_TEST_CMD =          8 
#INQ_SET_VALVES_OFF_CMD =        9 
        if INQ_DO_STATION_TEST_CMD==parm1 :
            #gFrame.uiConsole("Station Test started")
            doStationTestWithLogging()

#INQ_GET_CURRENT_CMD =          10
#INQ_GET_RAIN_SWITCH_CMD =      11
        if INQ_GET_RAIN_SWITCH_CMD==parm1 :
            cardRainSwitchTst()
            
#INQ_GET_SIM_CARD_NUMBER_CMD =  12
        if INQ_GET_SIM_CARD_NUMBER_CMD==parm1 :
            getModemInfo()

#INQ_SET_BEEPER_CMD =           13
        elif INQ_SET_BEEPER_CMD==parm1 :
            #hym.setBeep()
            cardBeepTst()
#INQ_GET_BUTTON_CMD =           14
#INQ_GET_SERIAL_NUMBER_CMD =    15
        elif INQ_GET_SERIAL_NUMBER_CMD==parm1 :
            line1 = hym.getSn()
            logging.info("GET_SERIAL_NUMBER_CMD: %s" % line1)   

#INQ_SET_SCREEN_CMD =           16
#INQ_TURN_ON_LEDS_CMD =         17
        elif INQ_TURN_ON_LEDS_CMD==parm1 :
            cardLedsTst()
#delete INQ_TURN_OFF_LEDS_CMD =        18
#INQ_WAIT_BUTTON_RELEASE_CMD =  19
#INQ_GET_SLOT_CMD =             20
#INQ_GET_FLOW_CMD =             21
        elif INQ_GET_FLOW_CMD==parm1 :
            cardFlowTst()
#INQ_SET_TERMINAL_STATUS_CMD =  22
#INQ_GET_SYSTEM_STATUS_CMD =    23
#INQ_SET_MODEM_STATUS_CMD =     24
#INQ_SET_J3_LOW_CMD =           25
#INQ_SET_J3_HIGH_HIGH_CMD =     26
#INQ_SET_J4_LOW_CMD =           27
#INQ_SET_J4_HIGH_CMD =          28
#INQ_TEST_RTC_CMD =             29
#INQ_TEST_EEPROM_CMD =          30
#INQ_GET_CAPTOUCH_STATUS_CMD =  31
#INQ_SET_CAPTOUCH_STATUS_CMD =  32

        if INQ_DO_CAPTOUCH_TEST_CMD==parm1 :
            gFrame.uiConsole("Key Test started")
            doKeyboardTest()

#INQ_SET_J5_LOW_CMD =           33
#INQ_SET_J5_HIGH_CMD =          34
#INQ_GET_FOUT_CMD =             35
#INQ_SEND_MODEM_STRING_CMD =    36
#INQ_MODEM_RESET_CMD =          37
#INQ_SET_FACTORY_CONFIG_CMD =   38
#INQ_GET_FACTORY_CONFIG_CMD =   39

#INQ_GET_READ_CONTEXT_CMD=   100
        elif INQ_GET_READ_CONTEXT_CMD==parm1 :
            #logging.info('Evt ConfigureRead')
            #respAttn=hym.hyAttn()
            cardSnCheck()
            readContext()
            
#INQ_SET_WRITE_CONTEXT_CMD=  101
        elif INQ_SET_WRITE_CONTEXT_CMD==parm1 :
            logging.info('Evt ConfigureWrite')
            cardSnCheck()
            writeContext()

# INQ_GET_CONFIGURE_SYSTEM_DETAILS_CMD= 102
        elif INQ_GET_CONFIGURE_SYSTEM_DETAILS_CMD==parm1 :
            cardSnCheck()
            respHwConfig=hym.getHwConfig()
            gFrame.uiConsole("card: "+str(gCustSn)+ ' '+str(respHwConfig),2)

# INQ_SET_DEFAULT_EEPROM_CMD= 103
        elif INQ_SET_DEFAULT_EEPROM_CMD==parm1 :
            cardSnCheck()
            setDefaultEeprom() #includes delay for restart then need to say hello

#INQ_CONFIGURE_SYSTEM_CMD 
        elif INQ_CONFIGURE_SYSTEM_CMD==parm1 :
            configureSystem()
        
# INQ_DO_ALL_SYSTESTS_CMD 
        elif INQ_DO_ALL_SYSTESTS_CMD==parm1 :
            cardSnCheck() 
            gFrame.uiConsole("card: "+str(gCustSn)+ ' Started ALL System Test',2)
            tstResults = allSystemTest()
            msgUser = "ALL Tests completed successfully"
            uiStatus = UiConsoleStatus_PASS
            print("INQ_DO_ALL_SYSTESTS_CMD "+str(tstResults))
            if tstResults != HSUCCESS :
                msgUser = "Not all tests passed, please correct, and then retest"
                uiStatus = UiConsoleStatus_FAIL
            gFrame.uiConsole( msgUser,uiStatus)
            
#INQ_READ_SERIALNUMBERS_CMD 
        elif INQ_READ_SERIALNUMBERS_CMD==parm1 :
            global gAsk_snA
            #cardSnCheck() 
            #gFrame.uiConsole("Future: This will use bar scanner to scan serial numbers. ",2)
            #This needs to have a dialog with all the cards input identified 
            # Then have the  
            #gFrame.UserDialogOK( 'Press OK when complete', caption = 'Scan SerialNumbers') 
            #ask_snA= gFrame.UserTextEntryDialog("msg scan/enter sn")
            #
            gFrame.uiConsole("Serial number Scanned: "+gAsk_snA,2)


def serial_ports_list():
    """ Lists serial port names

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(99)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    #print("serial_ports_list "+' '.join(result))
    return result
    
if __name__ == '__main__':
    #logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))
    #global gCustSn
    todaysDate =datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d')
    logFilename='wtsystest_log'+todaysDate+'.txt',
    parser = argparse.ArgumentParser(
                      epilog="Uses " +gCfgFileNameDefault
                      )
    parser.add_argument("-d","--dut", dest="dut",
                        help="device under test - .cfg must have section [DUT], other wises use default in .cfg "
                        )
    parser.add_argument("-l","--log", dest="loglevel",default="INFO", choices=['DEBUG','INFO', 'WARNING', 'ERROR', 'CRITICAL'],
                        help="log to "+str(logFilename)
                        )
    parser.add_argument("-u","--unlockui", dest="unlockui",
                        help="-u Unlock the UI so values can be modified "
                        )
    args = parser.parse_args()

    if "DEBUG"==args.loglevel:
        #DEBUG from logger and serial protocol SPY are combined at the terminal
        logger = logging.basicConfig(level=args.loglevel,
                                 )
    else:
        logger = logging.basicConfig(level=args.loglevel,
                                filename='wtsystest_log'+todaysDate+'.txt',
                                 )
    #logger = logging.basicConfig(level=args.loglevel)
    logging.info(sys.argv[0]+' '+ str(__version__))
    logging.info('Python '+sys.version)
    logging.info('wxPython '+wx.version()+',''serial '+serial.__version__+'\n')
    
    inUi_queue = queue.Queue(maxsize=20)  
    config = ConfigParser()
    config.sections()

    
    cfgFileName =gCfgFileNameDefault
    #if (1 < len(sys.argv)) :
    #    cmd_line_file = './'+sys.argv[1]
    #    if os.path.isfile(cmd_line_file) :
    #        cfgFileName=cmd_line_file
    #    else :
    #        print("Cmd line file "+cmd_line_file+" doesn't exist. Using default "+gCfgFileNameDefault)
    
    unlockui_bool=False
    try:
        with open(cfgFileName) as f:
            config.read(cfgFileName)
            #TODO test if sections exist
            if config.has_option('common', 'comport'):
                gSerialPortCom=(config.get('common', 'comport'))
            if args.dut is None:
                if config.has_option('common', 'dut'):
                    dut_specification=config.get('common', 'dut')
                else :
                    input("No default card 'dut' found. Please specify default. Press Enter when read...")
                    sys.exit()
            else:
                dut_specification=args.dut
            
            if config.has_option('common', 'version_rel'):
                gVerTgtRel=config.get('common', 'version_rel').split('.')  #expect two parts eg 7.10
                #logging.debug('common version_rel minimium '+str(gVerTgtRel))

            if config.has_option(dut_specification, 'Code1'):
                gCustPcCode1=config.get(dut_specification, 'Code1')
            else :
                input("No section ["+dut_specification+"] Code1 found in "+cfgFileName+" . Press Enter when read...")
                sys.exit()

            #configure station Current at default [common] then override if any specific [dut_specification]
            if config.has_option('common', 'stationtestmax_ma'):
                    gStationtestMax_ma=config.getint('common', 'stationtestmax_ma')
            if config.has_option('common', 'stationtestmin_ma'):
                    gStationtestMin_ma=config.getint('common', 'stationtestmin_ma')
            if config.has_option('common', 'mvtestmax_ma'):
                    gMvtestMax_ma=config.getint('common', 'mvtestmax_ma')
            if config.has_option('common', 'mvtestmin_ma'):
                    gMvtestMin_ma=config.getint('common', 'mvtestmin_ma')

            if config.has_option(dut_specification, 'stationtestmax_ma'):
                    gStationtestMax_ma=config.getint(dut_specification, 'stationtestmax_ma')
            if config.has_option(dut_specification, 'stationtestmin_ma'):
                    gStationtestMin_ma=config.getint(dut_specification, 'stationtestmin_ma')
            if config.has_option(dut_specification, 'mvtestmax_ma'):
                    gMvtestMax_ma=config.getint(dut_specification, 'mvtestmax_ma')
            if config.has_option(dut_specification, 'mvtestmin_ma'):
                    gMvtestMin_ma=config.getint(dut_specification, 'mvtestmin_ma')
                    
            if config.has_option(dut_specification, 'ControlBoardTest'):
                gControlBoardTest=config.getboolean(dut_specification, 'ControlBoardTest')             
            
            if config.has_option(dut_specification, 'StationKeysTest'):
                gStationKeysTest=config.getboolean(dut_specification, 'StationKeysTest')     
                
            if config.has_option(dut_specification, 'SerialNumberWr'):
                gSerialNumberWr=config.getboolean(dut_specification, 'SerialNumberWr') 

            if config.has_option(dut_specification, 'ModemSimTest'):
                gModemSimTest=config.getboolean(dut_specification, 'ModemSimTest') 

            if config.has_option(dut_specification, 'LedsTest'):
                gLedsTest=config.getboolean(dut_specification, 'LedsTest') 

            if config.has_option(dut_specification, 'BeepTest'):
                gBeepTest=config.getboolean(dut_specification, 'BeepTest') 

            if config.has_option(dut_specification, 'RainswitchTest'):
                gRainswitchTest=config.getboolean(dut_specification, 'RainswitchTest') 

            if config.has_option(dut_specification, 'KeyboardRead'):
                gKeyboardRead=config.getint(dut_specification, 'KeyboardRead') 

            if config.has_option(dut_specification, 'ValveTest'):
                gValveTest=config.getboolean(dut_specification, 'ValveTest')                

            if config.has_option(dut_specification, 'FlowTest'):
                gFlowTest=config.getboolean(dut_specification, 'FlowTest')
                                
            if config.has_option(dut_specification, 'KeyboardTestBeepOn'):
                #gCustDutIgnore=config.get(dut_specification, 'Key')
                #if gCustDutIgnore has "StationTestAuto"
                #print("gCustDutIgnore ")
                gKeyboardTestBeepOn=config.getboolean(dut_specification, 'KeyboardTestBeepOn')  
                
            if config.has_option(dut_specification, 'StationTestAutoOff'):
                #gCustDutIgnore=config.get(dut_specification, 'StationTestAutoOff')
                #if gCustDutIgnore has "StationTestAuto"
                #print("gCustDutIgnore ")
                gStationTestAuto=False
             
            unlockui='NO'
            if args.unlockui is None:
                if config.has_option('common', 'unlockui'):
                    unlockui=config.get('common', 'unlockui')
            else:
                unlockui=args.unlockui               

            if 'YES'==unlockui :
                unlockui_bool=True
                logging.info("UI unlocked")
                
            logging.info("parsing "+cfgFileName+" using "+dut_specification)
            #print(dut_specification+' serialport='+gSerialPortCom)
    except IOError as e:
        print("File doesn't exist: "+cfgFileName)
        sys.exit()
    #except :
    #    print("Error parsing "+cfgFileName)
    #    exit()
        
    with open(gHydrLogFileNam+todaysDate+'.txt','a',newline='') as gHydr_csv_file:
        app = wx.App(False) #stdout is cmdline
        gHydroLog=csv.writer(gHydr_csv_file, delimiter=',',
                            quotechar='"', quoting=csv.QUOTE_MINIMAL)
        #sttimeDay = datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d')
        sttimeSec = datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S')
        gHydroLog.writerow([todaysDate,sttimeSec]+["00000000"]+["Starting "+app.GetAppDisplayName()+' '+__version__])

        serialPortList = serial_ports_list()
        serialPortsNum=len(serialPortList)
        if 0 == serialPortsNum :
            exitMsg='No serials port found.'
            if '' != gSerialPortCom : 
                exitMsg +=' USB port '+gSerialPortCom+' specified in '+cfgFileName+':comport='+gSerialPortCom+' '
            exitMsg += ' Please insert USB serial connector'
            print(exitMsg)
            gHydroLog.writerow([todaysDate,sttimeSec]+["00000000"]+[exitMsg])
            input("Press Enter when read...")
            sys.exit()
        serialPortStr=','.join(serialPortList)
        uiComMsg = 'Found '+str(serialPortsNum)+'port(s): '+serialPortStr;
        print(uiComMsg)
        logging.info(uiComMsg)
        if '' == gSerialPortCom : 
            gSerialPortCom=serialPortList[0]
            uiComMsg ='No COM port specified, using first USB port: '+str(gSerialPortCom)
            print(uiComMsg)
            logging.info(uiComMsg)
                #print('Found',len(serialPortList),' ports: '+','.join(serialPortStr)
        else :
            if gSerialPortCom in serialPortList:
                uiComMsg='Using specified USB Port '+gSerialPortCom+' out of : ['+','.join(serialPortList)+']'
                print(uiComMsg)
            else:
                uiComMsg=' USB port '+gSerialPortCom+' specified in '+cfgFileName+':comport='+gSerialPortCom+'. Found: ['+','.join(serialPortList)+'] please update.'
                print(uiComMsg)
                input("Press Enter when read...")
                sys.exit()
                                
        logProtocol=''       
        if "DEBUG"==args.loglevel:
            logProtocol='spy://'
        #logProtocol='protocol_ftdi://'    
        #inter_byte_timeout > 0.02 as internally set to multiples 1/100 sec
        gSerHandle = serial.serial_for_url(logProtocol+gSerialPortCom, baudrate=57600, timeout=1,inter_byte_timeout=0.1)
        #gSerHandle = serial.serial_for_url(logProtocol+gSerialPortCom, baudrate=57600, timeout=1,inter_byte_timeout=0.1,rtscts=1) #doesn't work
        #gSerHandle = mySerial.serial_for_url('spy://'+gSerialPortCom, baudrate=57600, timeout=1,inter_byte_timeout=0.1)
        #gSerHandle = mySerial.serial_for_url(gSerialPortCom, baudrate=57600, timeout=1,inter_byte_timeout=0.02) #noDebug
        #print(gSerHandle.get_settings())


        #should be possible to read FTDI serial presence if cts tied to DUT +5V - hasn't worked so far
        #??ctsStatus = gSerHandle.cts()
        #ctsStatus = gSerHandle.getCTS()
        #logging.info('serial CTS '+str(ctsStatus))
        
        with serial.threaded.ReaderThread(gSerHandle, HYEC01 ) as hym:
            #print "Started ReaderThread.HYEC01" 
            worker = threading.Thread(target=inUi_thread)
            worker.setDaemon(True)
            worker.start()
            
 			#Set up hw specific opts
            if 'LC18' == dut_specification :
                hwModelEqu =HwModelLC18
            elif 'LC36' == dut_specification :
                hwModelEqu =HwModelLC36
            elif 'PRO3_H2O' == dut_specification :
                hwModelEqu =HwModel2W2G
            elif 'OPTIXR_H2O' == dut_specification :
                hwModelEqu =HwModel2W2G  
            elif '7000L01_TEST_BOARD' == dut_specification :
                hwModelEqu =HwModelLC18
                TestModelEqu =TestModelController_BRD
                gConfigureSystem=False
                gReadBoardSerialNum=False
            elif '7000L03_TEST_BOARD' == dut_specification :
                hwModelEqu =HwModelLC18
                TestModelEqu =TestModelCapactive_BRD
                gConfigureSystem=False
                gReadBoardSerialNum=False
            elif '706LC10_TEST_BOARD' == dut_specification :
                hwModelEqu =HwModelLC18
                TestModelEqu =TestModelCapactive_BRD
                gConfigureSystem=False
                gReadBoardSerialNum=False                    

            gFrame = Mywin(None)
            gFrame.Show(True)    
            titleAtStartup = gFrame.GetTitle()          
            gFrame.SetTitle(dut_specification+' : '+titleAtStartup+' ('+__version__+')')

            if not gLedsTest :
                gFrame.m_DoLedsTestBtn20.Enable(False)

            if not gBeepTest :
                gFrame.m_DoBeepTestBtn21.Enable(False)

            if 0== gKeyboardRead :
                gFrame.m_DoKeysBtn22.Enable(False)
                # ??Key Report Frame
                
            if not gRainswitchTest :
                gFrame.m_DoRainSwitchTestBtn19.Enable(False)                
            
            if not gValveTest :
                gFrame.m_DoStationTestBtn14.Enable(False)
            
            if not gFlowTest :
                gFrame.m_GetFlowBtn17.Enable(False)            

            if not gConfigureSystem :
                gFrame.m_SysTestConfigureSystemBtn19.Enable(False)
                
            if not gReadBoardSerialNum :
                gFrame.m_ReadBoardSerialNumbers19.Enable(False)

            if  TestModelController_BRD == TestModelEqu :
            #if  True : #TestModelController_BRD == TestModelEqu :
                startupDelay_sec =45
                gFrame.uiConsole('Delay '+str(startupDelay_sec) + 'secs for Controller startup')
                time.sleep(5)
                for lp in range(1,8):
                    gFrame.uiConsole('zzz '+str(startupDelay_sec-(lp*5)) + 'secs')
                    time.sleep(5)
                
            checkDutPresent()
            
            gCustSn = hym.getSn()
            if '' == gCustSn :
                gCustSn = '0000000'
            timeoutCheck(gCustSn,"Please reset DUT, timeout on startup Read SN")
            #respHwConfig=hym.getHwConfig()
            #hw1Str = list(respHwConfig)
            #determine whether LC18 or LC36 from CPU Model ID
            #hwModelEqu - already global 
                
            sttimeSec = datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S')
            respVersionDut=hym.getVersion()
            outputVersion = ''
            if 'None' != respVersionDut  :
                VersionDut=json.loads(respVersionDut)
                VerDutStr=VersionDut["VERSION"]
                #expect MJ.MN - will break when gets to "10"
                gVerDutList = (VerDutStr.split('.',2))
                gVerDutMjInt=int(gVerDutList[0])
                gVerDutMnInt=int(gVerDutList[1]) 
                if (gVerDutMjInt < int(gVerTgtRel[0])) or (gVerDutMnInt<int(gVerTgtRel[1])) :
                    outputVersion = 'Wrong Version '+str(gVerDutMjInt)+'.'+str(gVerDutMnInt)+' Upgrade to '+str(gVerTgtRel[0])+'.'+str(gVerTgtRel[1])
                else :
                    outputVersion = 'Version '+str(gVerDutMjInt)+'.'+str(gVerDutMnInt)+' ['+str(VerDutStr)+']'
                    #gFrame.uiConsole('ver:'+str(gVerDutMjInt)+'.'+str(gVerDutMnInt))

            else :
                outputVersion = '  Version not readable. Please verify output version '
                if len(gVerTgtRel)>1 :
                    outputVersion +=' is at least '+ str(gVerTgtRel[0])+'.'+ str(gVerTgtRel[1])
                
            gFrame.uiConsole('card: '+gCustSn+' connected to '+dut_specification+" as "+HwModelTxt[hwModelEqu]+" "+outputVersion+" on "+gSerialPortCom,
                             status=2) 
            gHydr_csv_file.flush()
            
            #Write out the serial numeber for an external program to read and print a label
            fileSerialNumber_txt = 'serialNumber.txt'
            #fut fileSysConfig_json =   'sysConfig.json'
            try :
                fsntxt = open(fileSerialNumber_txt,"w+")                
                fsntxt.write(gCustSn)
                fsntxt.close()
            except IOError:
                gFrame.uiConsole("can't open:"+fileSerialNumber_txt+ 'Another program has it, or reboot.', status=2) 

            #respSystemState=hym.getTerminalStatus()
            #gFrame.uiConsole('status: '+str(respSystemState),     status=2)   
            if "DEBUG"==args.loglevel:
                respBuildTime=hym.getBuildTime() # on debug display
            #Config per system
            gFrame.m_confPcCode1.SetLabelText(gCustPcCode1)
            if True==unlockui_bool :
                gFrame.m_confPcCode1.SetEditable(True)
                gFrame.m_confPcCode2.SetEditable(True)
                gFrame.m_conf800Num.SetEditable(True)
                gFrame.m_confCompanyName.SetEditable(True)

			#if need to read Modem then need to delay 30sec         
            if gModemSimTest :
                gFrame.uiConsole("Now reading modem SIM s/n ~ about 20secs")    
                gModemSimSn = hym.getSimCardNumber()
                #if not gModemSimSn  : #gModemSimSn is None:
                if "0" == str(gModemSimSn)  : #gModemSimSn is None:
                    gFrame.uiConsole("FAIL reading modem SIM s/n: "+str(gModemSimSn),2)
                else :    
                    gFrame.uiConsole("modem SIM s/n: "+str(gModemSimSn),2)

            gFrame.Maximize(True)
            stationValveReportInit()
            app.MainLoop()

    #print "mainExit"
    if gHydr_csv_file:
        gHydr_csv_file.close()
    
    #input("Press Enter to exit...") #this slows down cycling
    #inUi_queue.task_done()
    

