#! /usr/bin/env python
# encoding: utf-8
"""
Hy command protocol for comms to the PIC32 Hydropoint Embedded Processor

Copyright Hydropoint Data Systems Inc 2018
Neil Hancock 2017 Dec 1st

The initial PIC32 "debug" interface appears to be modelled on an AT method. 
https://en.wikipedia.org/wiki/Hayes_command_set
http://www.itu.int/rec/T-REC-V.250-200307-I/en
https://github.com/pyserial/pyserial/tree/master/examples/at_protocol.py
"""
from __future__ import print_function

import sys
#from pip.commands import get_similar_commands
from test.test_dummy_thread import DELAY
sys.path.insert(0, '..')

import logging
import serial
import serial.threaded  as myThreaded
import threading
import time
    
try:
    import queue
except ImportError:
    import Queue as queue

# The following is mapped to weathertrack\software\wtlib\comp\hw\hw_triton_X.X\main.c
hyAttentionCmd = 'Hello' 

GET_CONTROLLER_STATUS_CMD = '1'
GET_TERMINAL_STATUS_CMD =   '2'
GET_MODEM_STATUS_CMD =      '3'
SET_CONTROLLER_STATUS_CMD = '4'
SET_SYSTEM_STATUS_CMD =     '5' 
SET_1800_NUMBER_CMD =       '6' 
GET_1800_NUMBER_CMD =       '7' 
SET_VALVE_ON_CMD =          '8' 
SET_VALVES_OFF_CMD =        '9' 
GET_CURRENT_CMD =          '10'
GET_RAIN_SWITCH_CMD =      '11'
GET_SIM_CARD_NUMBER_CMD =  '12'
SET_BEEPER_CMD =           '13'
GET_BUTTON_CMD =           '14'
GET_SERIAL_NUMBER_CMD =    '15'
SET_SCREEN_CMD =           '16'
TURN_ON_LEDS_CMD =         '17'
TURN_OFF_LEDS_CMD =        '18'
WAIT_BUTTON_RELEASE_CMD =  '19'
GET_SLOT_CMD =             '20'
GET_FLOW_CMD =             '21'
SET_TERMINAL_STATUS_CMD =  '22'
GET_SYSTEM_STATUS_CMD =    '23'
SET_MODEM_STATUS_CMD =     '24'
SET_J3_LOW_CMD =           '25'
SET_J3_HIGH_HIGH_CMD =     '26'
SET_J4_LOW_CMD =           '27'
SET_J4_HIGH_CMD =          '28'
TEST_RTC_CMD =             '29'
TEST_EEPROM_CMD =          '30'
GET_CAPTOUCH_STATUS_CMD =  '31'
SET_CAPTOUCH_STATUS_CMD =  '32'
SET_J5_LOW_CMD =           '33'
SET_J5_HIGH_CMD =          '34'
GET_FOUT_CMD =             '35'
SEND_MODEM_STRING_CMD =    '36'
MODEM_RESET_CMD =          '37'
SET_FACTORY_CONFIG_CMD =   '38'
GET_FACTORY_CONFIG_CMD =   '39'
GET_HW_CONFIG =            '40'
GET_BUILD_TIME =           '41'    
SET_EEPROM_DEFAULT =       '42'  
GET_CURRENT_AVG_CMD =      '43'
GET_VERSION_CMD =          '44'
GET_CURRENT_2W2G_CMD =      '45'

class HYException(Exception):
    pass

class HYProtocol(myThreaded.LineReader):
    #TERMINATOR = b'\r\n'
    #TERMINATOR is used on the output to terminate the command buffer to the serial handler in the PIC32
    TERMINATOR = b'\0'
    #TERMINATOR_RX = '\0'
    TERMINATOR_RX = b'\0'
    
    def __init__(self):
        super(HYProtocol, self).__init__()
        self.alive = True
        self.responses = queue.Queue()
        self.events = queue.Queue()
        self._event_thread = threading.Thread(target=self._run_event)
        self._event_thread.daemon = True
        self._event_thread.name = 'hy-event'
        self._event_thread.start()
        self.lock = threading.Lock()
        self.buffer = bytearray()
        self.state = 0 #0=cmd 1=data
        self.start_msgtime = time.monotonic()

    def stop(self):
        """
        Stop the event processing thread, abort pending commands, if any.
        """
        self.alive = False
        self.events.put(None)
        self.responses.put('<exit>')

    def _run_event(self):
        """
        Process events in a separate thread so that input thread is not
        blocked.
        """
        while self.alive:
            try:
                self.handle_event(self.events.get())
            except:
                logging.exception('_run_event')

    def handle_line(self, line):
        """
        Handle input from serial port, check for events.
        """
        #if line.startswith('+'):
        #    self.events.put(line)
        #else:
        #    self.responses.put(line)
        self.buffer.extend(line)
        #print('handle_line1: '+str(self.buffer)) 
        #print('handle_line: '+line+':'+" ".join("{:02x}".format(ord(c)) for c in line))
        #print('handle_line: '+line+':'+" ".join("{:02x}".format(str(c) for c in line))
        if 0 == self.state :
            while self.TERMINATOR_RX in self.buffer:
                packet, self.buffer = self.buffer.split(self.TERMINATOR_RX, 1)
                #print('handle_line20: '+str(packet)) 
                self.responses.put(packet.decode(self.ENCODING, self.UNICODE_HANDLING)) #into Queue
        else:
            buf_len = len(self.buffer)
            elapsed_time_sec = time.monotonic()- self.start_msgtime
            #print('handle_line21:'+str(buf_len)+':'+ '('+str(elapsed_time_sec)+') '+str(self.buffer)) 
            if buf_len >2 : #new packet - should be timeout :
                #packet, self.buffer = self.buffer.split(self.TERMINATOR_RX, 1)
                packet = self.buffer
                #print(packet)
                self.responses.put(packet.decode(self.ENCODING, self.UNICODE_HANDLING)) #into Queue
                self.buffer =bytearray()

            
        # else wait for timeout to collect packet


    def handle_event(self, event):
        """
        Spontaneous message received.
        """
        print('event received:', event)

    def command(self, command, response='OK', timeout=5):
        """
        Set an HY command and wait for the response.
        """
        with self.lock:  # ensure that just one thread is sending commands at once
            self.write_line(command)
            lines = []
            self.state = 0 #0=cmd 1=data
            self.start_timer()
            while True:
                try:
                    line = self.responses.get(timeout=timeout)
                    #print ('HypCommand:'+str(line))#type(line).__name__)
                    #print("%s ->(%s) %r" % (command, response,line))
                    #line2 = line[:2]
                    if line[:2] == response:
                    #if response == line.split(self.TERMINATOR_RX, 1) :
                        return lines
                    else:
                        lines.append(line)
                except queue.Empty:
                    #raise HYException('AT command timeout ({!r})'.format(command))
                    logging.info('HY command timeout ({!r})'.format(command))
                    lines = 'timeout'
                    return lines
                    break

    def command_with_data(self, command, response='OK', timeout=2,sn_timeout_sec = 5):
        """
        Set an HY command and wait for the response and collect data
        """
        with self.lock:  # ensure that just one thread is sending commands at once
            self.write_line(command)
            lines = []
            self.state = 1 #0=cmd 1=data
            self.start_timer()
            #print("cmddata start timeout:" +str(timeout) +"  sn_timeout_sec:" +str(sn_timeout_sec))
            while True:
                try:
                    lineAll = self.responses.get(timeout=timeout) 
                    #logging.debug("cmdwithdata1:%s -> %r" % (command, lineAll)+' '+response+' '+lineAll[:2])                    
                    if lineAll[:2] == response:
                        line = str(lineAll[3:] )
                        #index0 = line.find(self.TERMINATOR_RX)
                        #if (-1 != index0):
                        #    line = line[:index0-1]
                        #line = line.split(self.TERMINATOR_RX, 1) -doesn't like bytes
                        #print("cmddata2  %r" % str(line))
                        #return lines
                        
                        #the following isn't really used - seems to relate to timeouts
                        while True:
                            try:
                                #line = self.responses.get(timeout=sn_timeout_sec)
                                #print("cmddata3 SnEv %r" % (line))
                                #lines.append(line)
                                elapsed_time_sec = time.monotonic()- self.start_msgtime
                                #print("HyCwd: %s -> %r " % (line,elapsed_time_sec))
                                #print('HyCwd: '+line+' sz'+str(len(line))+" elapsed "+str(elapsed_time_sec))
                                return line
                            except queue.Empty:
                                logging.warning('HY command data timeout ({!r})'.format(command))
                                break
                        else:
                            lines.append(line)
                    else:
                        logging.warning("HY cmddata3: Not found %s -> %r" % (response, lineAll))
                except queue.Empty:
                    #raise HYException('AT command timeout ({!r})'.format(command))
                    logging.warning('HY command data timeout ({!r})'.format(command))
                    break

    def command_with_sn(self, command, response='OK', timeout=2,sn_timeout_sec = 30):
        """
        Set an HY command and wait for the response and collect data
        """
        with self.lock:  # ensure that just one thread is sending commands at once
            self.write_line(command)
            lines = []
            self.state = 1 #0=cmd 1=data
            self.start_timer()
            print("cmdsn start timeout:" +str(timeout) +"  sn_timeout_sec:" +str(sn_timeout_sec))
            while True:
                try:
                    lineAll = self.responses.get(timeout=timeout) 
                    #logging.debug("cmdwithdata1:%s -> %r" % (command, lineAll)+' '+response+' '+lineAll[:2])                    
                    if lineAll[:2] == response:
                        line = str(lineAll[3:] )
                        #index0 = line.find(self.TERMINATOR_RX)
                        #if (-1 != index0):
                        #    line = line[:index0-1]
                        #line = line.split(self.TERMINATOR_RX, 1) -doesn't like bytes
                        print("cmdsn2  %r" % str(line))
                        #return lines
                        while True:
                            try:
                                line = self.responses.get(timeout=sn_timeout_sec)
                                print("cmdsn3 SnEv %r" % str(line))
                                #lines.append(line)
                                elapsed_time_sec = time.monotonic()- self.start_msgtime
                                #print("HyCwd: %s -> %r " % (line,elapsed_time_sec))
                                #print('HyCwd: '+line+' sz'+str(len(line))+" elapsed "+str(elapsed_time_sec))
                                return line
                            except queue.Empty:
                                logging.warning('HY command data timeout ({!r})'.format(command))
                                break
                        else:
                            lines.append(line)
                    else:
                        logging.warning("HY cmddata3: Not found %s -> %r" % (response, lineAll))
                except queue.Empty:
                    #raise HYException('AT command timeout ({!r})'.format(command))
                    logging.warning('HY command data timeout ({!r})'.format(command))
                    break
                
    def snd_data(self, payload ):
        """
        Set an HY data and no response.
        """
        with self.lock:  # ensure that just one thread is sending commands at once
            self.write_raw(payload)


    def get_data(self, timeout=2 ):
        """
        Wait expected data.
        """
        lineResp = self.responses.get(timeout=timeout) 
        #logging.debug("get_data1: "+str(lineResp))
        return lineResp
            
    def start_timer(self):
        self.start_msgtime = time.monotonic()

class HYEC01(HYProtocol):
    """
    Communication with HYEC01 Hydropoint Embedded Controller 01 module.
    <leftover>
    Some commands do not respond with OK but with a '+...' line. This is
    implemented via command_with_event_response and handle_event, because
    '+...' lines are also used for real events.
    """

    def __init__(self):
        super(HYEC01, self).__init__()
        self.event_responses = queue.Queue()
        self._awaiting_response_for = None

    def connection_made(self, transport):
        super(HYEC01, self).connection_made(transport)
        # our adapter enables the module with RTS=low
        self.transport.serial.rts = False
        time.sleep(0.3)
        self.transport.serial.reset_input_buffer()

    if 0:
     def handle_event(self, event):
        """Handle events and command responses starting with '+...'"""

        if event.startswith('+RRBDRES') and self._awaiting_response_for.startswith('AT+JRBD'):
            rev = event[9:9 + 12]
            mac = ':'.join('{:02X}'.format(ord(x)) for x in rev.decode('hex')[::-1])
            self.event_responses.put(mac)
        else:
            logging.warning('unhandled event: {!r}'.format(event))

    if 0:
     def command_with_event_response(self, command):
        """Send a command that responds with '+...' line"""
        with self.lock:  # ensure that just one thread is sending commands at once
            self._awaiting_response_for = command
            self.transport.write(b'{}\r\n'.format(command.encode(self.ENCODING, self.UNICODE_HANDLING)))
            #nhself.transport.write(b'{}\r\n'.format(command))
            response = self.event_responses.get()
            self._awaiting_response_for = None
            return response

    # - - -commands - weathertrak\software\wtlib\comp\hw\hw_triton_7.7\main.c
    # Hello - 1st time puts into test, subsequently causes OK response
    # RESET triggerWatchdogReset
    # EXIT start wt_main.
    # 1 2 .. 39
    

    def reset(self):
        self.command("RESET")      # SW-Reset Hyd module

    def exit(self):
        self.command("EXIT")      # SW-Exit Hyd module
        
    def hyAttn(self):
        return self.command(hyAttentionCmd )
        

    def getControllerStatus(self):
        self.command(GET_CONTROLLER_STATUS_CMD)
        #how to catch response
        
    def getTerminalStatus(self):
        return self.command_with_data(GET_TERMINAL_STATUS_CMD)

    #GET_MODEM_STATUS_CMD =      '3'
    def getModemStatus(self):
        return self.command_with_data(GET_MODEM_STATUS_CMD)
    
    #SET_CONTROLLER_STATUS_CMD = '4'
    def setControllerStatus(self,status=0):
        self.command(SET_CONTROLLER_STATUS_CMD)
        self.command(status)
                
    #SET_SYSTEM_STATUS_CMD =     '5' 
    def setSystemStatus(self,status=0):
        self.command()
        self.command(status)
        
    #SET_1800_NUMBER_CMD =       '6' 
    # SETS supplied string into EEPROM - no test for length, becareful
    def set1800Number(self,status='1-800-362-8774'):
        self.command(SET_1800_NUMBER_CMD)
        self.command(status)
        
    #read 1800_NUMBER from EEPROM 
    def get1800Number(self):
        return self.command_with_data(GET_1800_NUMBER_CMD)
    
    # set all valves off
    def setValvesOff(self):
        self.command(SET_VALVES_OFF_CMD)    

    # set specified valive ON, input ascii str: 1-37 ,depending on system
    # times out if no valve to drive
    def setValveOn(self,valveNum='1'):
        self.command(SET_VALVE_ON_CMD)
        #print("setValveOn="+str(valveNum))
        self.command(str(valveNum))

    # Read HW_IO_GetCurrent returns string
    # read multiple times, finding difference between max and min, then for mA
    #mult LC18/36:2.31, Pro3 adapter:4.48, Pro3 2-wire:1.12,
    def getCurrentSampleAdc(self):
        valveCurrentStr_mA = self.command_with_data(GET_CURRENT_CMD)
        if not valveCurrentStr_mA :
            #Can happen if card turned off. should create exceptionmock it up so doesn't do TypeError
            return 0
        valveCurrent_Len = len(valveCurrentStr_mA)
        #print("getCurrent="+valveCurrentStr_mA+'/'+str(valveCurrent_Len))
        if valveCurrent_Len < 2 :
            return 0
        return int(valveCurrentStr_mA[:(valveCurrent_Len-1)],16)
        #return int(valveCurrentStr_mA.strip(0x00),16)

    # Read HW_IO_GetAverageCurrent returns string
    # read once time,  then for mA
    # mult LC36:2.31, Pro3 adapter:4.48, Pro3 2-wire:1.12,    
    def getCurrentAvgAdc(self): #TODO verify reads mA instead of ADC
        valveCurrentStr_mA = self.command_with_data(GET_CURRENT_AVG_CMD)
        if not valveCurrentStr_mA :
            #Can happen if card turned off. should create exceptionmock it up so doesn't do TypeError
            return None
        #print("getCurrentAvgAdc="+valveCurrentStr_mA)
        valveCurrent_Len = len(valveCurrentStr_mA)
        return int(valveCurrentStr_mA[:(valveCurrent_Len-1)],16)

    # Read HW_IO_GetBaseline2WireSolenoidCurrent returns string
    # read once time,  then for mA    
    def getCurrent2w2g(self):
        valveCurrentStr_mA = self.command_with_data(GET_CURRENT_2W2G_CMD)
        if not valveCurrentStr_mA :
            #Can happen if card turned off. should create exceptionmock it up so doesn't do TypeError
            return None
        #print("getCurrent2W2G="+valveCurrentStr_mA)
        valveCurrent_Len = len(valveCurrentStr_mA)
        return int(valveCurrentStr_mA[:(valveCurrent_Len-1)],16)

    # Read RAIN_SWITCH returns '1' or '0' or if None '2'
    def getRainSwitch(self):
        resp = self.command_with_data(GET_RAIN_SWITCH_CMD)
        if resp is None:
            resp = '2'
        return resp
    
    #Reads SIM_CARD_NUMBER and returns string, 
    # There are number of types of modems and not all are supported
    def getSimCardNumber(self):
        resp= self.command_with_sn(GET_SIM_CARD_NUMBER_CMD,timeout=60)
        
        if resp is None:
            return "Failed to read SIM!"
            
        resp_len = len(resp)
        logging.debug("getSimCardNumber: full %r" % str(resp)+" ("+str(resp_len)+")")  
        #expect len =44 '\r\n\r\n+CRSM: 144,0,98101440729040135974\r\n\r\nOK\x00'
        if resp_len >30 :
            resp2 =resp[17:(resp_len-7)]
            logging.debug("getSimCardNumber: parsed %r" % str(resp2))  
        else :
            resp2 = "0"
            logging.debug("getSimCardNumber: invalid set %r" % str(resp2))  
        #logging.debug("getModemSn2 " +resp2+" ;")

        return resp2
   
    def setBeep(self,state=2):
        self.command(SET_BEEPER_CMD)
        if 0==state :
            self.command("0")
        elif 1==state:      
            self.command("1")
        else:
            self.command("1")
            time.sleep(0.01)
            self.command(SET_BEEPER_CMD)
            self.command("0")             
            
    #get button from keyboard? 
    # returns integer 0x01-0x80,or if not readable 0xff
    def getButton(self): #-> string: 
        return self.command_with_data(GET_BUTTON_CMD)
    
    #get SerialNumber and returns string[8] '12345678'
    def getSn(self):  #-> string: 
        #returns with two parts
        #try
        resp = str(self.command_with_data(GET_SERIAL_NUMBER_CMD))
        logging.debug("getSn %r" % (resp))
        resp_len = len(resp)
        #print("getSn "+resp+" len" +str(resp_len)+" type"+ str(type(resp)))
        resp2 =resp[:(resp_len-1)]
        #Exception
        return resp2

    # SET_SCREEN - initialize?
    def setScreen(self):
        self.command(SET_SCREEN_CMD)

    # Set all LEDS 0=OFF or 1=ON
    def setLeds(self,state=0):
        if 0==state :
            self.command(TURN_OFF_LEDS_CMD)
        else :
            self.command(TURN_ON_LEDS_CMD) 

    # WAIT_BUTTON_RELEASE or timeout ~ 500mS. Response is alays OK
    def waitButtonRelease(self):
        self.command(WAIT_BUTTON_RELEASE_CMD)
    
    #GET_SLOT, return ASCII Num 'slot value' or 'FAIL<num>'
    def getSlot(self,slot_num):
        self.command(GET_SLOT_CMD)
        return self.command_with_data(str(slot_num))
    
    #GET_FLOW returns int 0x<flow>
    def getFlow(self,flowNum=0):
        #flowStr = self.command_with_data(GET_FLOW_CMD)
        self.command(GET_FLOW_CMD)
        flowStr=self.command_with_data(str(flowNum))
        #flowStr=self.get_data()
        if not flowStr :
            #Can happen if card turned off. should create exceptionmock it up so doesn't do TypeError
            return 0
        flowStr_Len = len(flowStr)
        #print("getFlow="+flowStr+'/'+str(flowStr_Len))
        return int(flowStr[:(flowStr_Len-1)],16)
    
    #SET_TERMINAL_STATUS to HW_IO_SetTerminalBoardStatus(<status str>
    def setTerminalStatus(self,status='000'): # needs more work
        self.command()
        self.command(status)
    
    #GET_SYSTEM_STATUS  returns one hex byte 
    def getSystemStatus(self):
        return self.command_with_data(GET_SYSTEM_STATUS_CMD)
    
    #SET_MODEM_STATUS - programs modem
    # status='0'  or '1',  sends to modem AT+MDC=<0/1>\r"
    def setModemStatus(self,status='0'):
        self.command()
        self.command(status)
    
    #SET_J3 state=0 low, state=1 high
    def setJ3(self,state=0):
        if 0==state :
            self.command(SET_J3_LOW_CMD)
        else :
            self.command(SET_J3_HIGH_HIGH_CMD)  
            
    #SET_J4 state=0 low, state=1 high
    def setJ4(self,state=0):
        if 0==state :
            self.command(SET_J4_LOW_CMD)
        else :
            self.command(SET_J4_HIGH_CMD)  

    #TEST_RTC, returns 'PASS' or 'x'
    def testRtc(self):
        rsp= self.command_with_data(TEST_RTC_CMD)
        #EEPROM test seems to take 200mS, and it times out.
        if not rsp :
            rsp = self.get_data()
        return rsp
    
    #TEST_EEPROM returns 'PASS' or 'x'
    def testEeprom(self):
        rsp= self.command_with_data(TEST_EEPROM_CMD)
        #EEPROM test seems to take 200mS, and it times out.
        if not rsp :
            rsp = self.get_data()
        return rsp

    #GET_CAPTOUCH_STATUS  returns byte 0-256
    def getCaptouchStatus(self):
        return self.command_with_data(GET_CAPTOUCH_STATUS_CMD)
    
    #SET_CAPTOUCH_STATUS, send status one byte
    def setCaptouchStatus(self,status=0):
        self.command(SET_CAPTOUCH_STATUS_CMD)
        self.command(status)
        
    #SET_J5 state=0 low, state=1 high
    def setJ5(self,state=0):
        if 0==state :
            self.command(SET_J5_LOW_CMD )
        else :
            self.command(SET_J5_HIGH_CMD)  
            
    #GET_FOUT returns one byte HW_IO_GetFout()
    def getFout(self):
        return self.command_with_data(GET_FOUT_CMD)
    
    #SEND_MODEM_STRING
    def sendModemString(self,cmdStr='AT'):
        self.command(SEND_MODEM_STRING_CMD)
        #may need to catch response as in getModemSn
        resp = str(self.command_with_data(cmdStr))
        # expect string to have \r\n\r\n <sn> \r\n\r\nOK
        logging.debug("sendModemStr1: %r" % resp)  
        resp_len = len(resp)
        resp2 =resp[4:(resp_len-7)]
        #logging.debug("getModemSn2 " +resp2+" ;")
        return resp2
        
    def getModemSn(self,cmdStr='AT+CGSN'):
        self.command(SEND_MODEM_STRING_CMD)
        resp = str(self.command_with_data(cmdStr))
        # expect string to have \r\n\r\n <sn> \r\n\r\nOK
        logging.debug("getModemSn1: %r" % resp)  
        resp_len = len(resp)
        resp2 =resp[4:(resp_len-7)]
        #logging.debug("getModemSn2 " +resp2+" ;")
        return resp2
    
    def getModemATresp(self,cmdStr='AT'): 
        #
        self.command(SEND_MODEM_STRING_CMD)
        resp = str(self.command_with_data(cmdStr))
        # expect string to have \r\n\r\n <resp> \r\n\r\nOK
        logging.debug("getModemResp: %r" % resp)  
        resp_len = len(resp)
        resp2 =resp[4:(resp_len-7)]
        logging.debug("getModemResp " +resp2+" ;")
        return resp2

    #MODEM_RESET
    def modemReset(self):
        self.command(MODEM_RESET_CMD)
            
    # Data struct wt_eeprom.h:factory_struct.h
    # eg dev\weathetrak\software\wtlib\comp\blob\blob_triton_8.2\wt_eeprom.h
    #Used in wt_app.h  
    #eg dev\weathertrak\software\stlib\mailine_triton_7.7\wt_app.h
    #SET_FACTORY_CONFIG_CMD =   '38'
    def setFactoryConfig(self,fc_str,status=0):
        resp=self.command(SET_FACTORY_CONFIG_CMD)
        #print('sFC1:',resp1)
        #Bug: after 1st pass through, always a turn left in resp1, only look for specific error to prevent sending read size
        #if resp != 'timeout':
        if not resp :
            resp=self.command('&')
            #print('sFC2:',resp2)
            #if 'timeout' != resp:
            if not resp:
                resp=self.snd_data(fc_str)
            
        #self.command(status)
        return resp
    
    #GET_FACTORY_CONFIG_CMD =   '39'               
    def getFactoryConfig(self,status=0):
        factConfig = bytearray() 
        resp1 = self.command(GET_FACTORY_CONFIG_CMD)
        listResp = ''.join(resp1)
        #print('gFc1:',*resp1, sep=', ')
        logging.debug('gFc2:'+listResp)
        #Bug: after 1st pass through, always a turd left in resp1, only look for specific error to prevent sending read size
        #if resp1 != 'timeout':
        #if resp1 is None:
        if not resp1 :
            #factConfig=self.command_with_data(SET_FACTORY_CONFIG_CMD)
            factConfig=self.command_with_data('&') #0x26 size of expected response
            return factConfig
        else :
            #print('gFcNone:',str(type(resp1)))
            return resp1

    def getHwConfig(self): # Objective is for data to be in JSON format - but needs some checking
        #factConfig = bytearray() 
        resp = str(self.command_with_data(GET_HW_CONFIG))
        logging.debug("getHwConfig %r" % (resp))
        resp_len = len(resp)
        #print("getHwConfig "+resp+" len" +str(resp_len)+" type"+ str(type(resp)))
        resp2 =resp[:(resp_len-1)]
        #Exception
        return resp2
        #Bug: after 1st pass through, always a turd left in resp1, only look for specific error to prevent sending read size
        #if resp1 != 'timeout':
        #if resp1 is None:

    def getBuildTime(self):  #-> string: 
        #returns with two parts
        #try
        resp = str(self.command_with_data(GET_BUILD_TIME))
        logging.debug("getBuildTime %r" % (resp))
        resp_len = len(resp)
        #print("getBuildTime "+resp+" len" +str(resp_len)+" type"+ str(type(resp)))
        resp2 =resp[:(resp_len-1)]
        #Exception
        return resp2

    def setEepromDefault(self):
        self.command(SET_EEPROM_DEFAULT)


    def getVersion(self):  #-> string: 
        #returns with two parts
        #try
        resp = str(self.command_with_data(GET_VERSION_CMD))
        logging.debug("getVersionCmd %r" % (resp))
        if 'None' == resp :
            return resp
        resp_len = len(resp)
        resp2 =resp[:(resp_len-1)]
        #Exception
        return resp2
# test - edit for serial - not derived automatically
if __name__ == '__main__':
    
    ser = serial.serial_for_url('spy://COM10', baudrate=57600, timeout=1)
    #~ ser = serial.Serial('COM1', baudrate=115200, timeout=1)

    with serial.threaded.ReaderThread(ser, HYEC01) as hym:
        while True:
            hym.hyAttn()
            #hyd_module.setBeep()
            line1 = hym.getSn()
            print("Sn=%s" % line1)
            hym.getFactoryConfig()
            #line1 = hym.getHwConfig()
            #print("HwConfig=%s" % line1)
            #line1 = hym.getBuildTime()
            #print("BuildTime=%s" % line1)

            #bt_module.reset()
            time.sleep(1)
            
        print("Complete")
        #print("MAC address is", hyd_module.get_mac_address())
